﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace Logfile_Writing
{
    class Lock
    {
        private readonly object LockObject = new object();
        public void WriteLog(string message)
        {
            if (!File.Exists("alllogs_1.txt"))
            {
                StreamWriter sw = new StreamWriter("alllogs_1.txt");
                sw.Flush();
                sw.Close();
            }
            lock (LockObject)
            {
                while (true)
                {
                    Monitor.Enter(LockObject);//lock does automatically monitor enter and exit
                    StreamWriter sw = new StreamWriter(WritableFile(), true);
                    sw.WriteLine(DateTime.Now + " " + message);
                    sw.Flush();
                    sw.Close();
                    Monitor.Exit(LockObject);


                }

            }
        }
        public static string WritableFile()
        {

            int fileIndex = 0;
            string wirtableFile = string.Empty;
            while (wirtableFile == string.Empty)
            {
                fileIndex++;
                wirtableFile = Linecount(fileIndex);
            }
            return wirtableFile;
        }


        private static string Linecount(int index)
        {
            object locker = new object();
            lock (locker)
            {
                int counter = 1;
                using (StreamReader countReader = new StreamReader("alllogs_" + index + ".txt"))
                {
                    while (countReader.ReadLine() != null)
                        counter++;

                    if (counter == 10)
                    {
                        StreamWriter sw = new StreamWriter("alllogs_" + index + ".txt");
                        sw.Flush();
                        sw.Close();
                        index++;

                    }
                }
                if (counter < 11)
                    return "alllogs_" + index + ".txt";
                else
                    return string.Empty;
            }
        }

    }
}
