﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace CAD_Decorator
{
    class MyCircle:AShape
    {
        Ellipse tempE = new Ellipse();
        public override Shape s
        {
            get { return tempE; }
        }

        public override bool IsSelected
        {
            get { return IsSelected; }
            set { IsSelected = false; }
        }

        public MyCircle(Ellipse s, int x, int y):base(s)
        {
            this.tempE = new Ellipse { Height = s.Height, Width = s.Width, Stroke = System.Windows.Media.Brushes.Black, StrokeThickness = 3 };
            Canvas.SetTop(tempE, y - s.Height/2);
            Canvas.SetLeft(tempE, x - s.Height/2);
        }
    }
}
