﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD_Decorator
{
    abstract class AShapeDecorator:AComponent
    {
        AShape shape;

        public AShapeDecorator(AShape Form)
        {
            this.shape = Form;
        }
    }
}
