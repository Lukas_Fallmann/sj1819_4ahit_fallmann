﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CAD_Decorator
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer t = new DispatcherTimer();
        
        StackPanel lineoptions = new StackPanel();
        StackPanel rectoptions = new StackPanel();
        StackPanel circoptions = new StackPanel();
        public MainWindow()
        {
            t.Interval = new TimeSpan(10);
            t.Tick += new EventHandler(TimerCallback);
            InitializeComponent();
        }
        private void TimerCallback(object sender, EventArgs e)
        {
            List<Shape> shapes = new List<Shape>();
            foreach (Shape s in Drawingspace.Children)
            {
                shapes.Add(s);
            }
            Drawingspace.Children.Clear();
            foreach (Shape s in shapes)
            {
                Drawingspace.Children.Add(s);
            }
        }
        private void BTLin_Click(object sender, RoutedEventArgs e)
        {
            BTLin.IsEnabled = false;
            lineoptions.Children.RemoveRange(0, lineoptions.Children.Count);
            TextBox startx = new TextBox
            {
                SelectedText = "StartValue x",
                Tag = "temp",

            };
            lineoptions.Children.Add(startx);

            TextBox starty = new TextBox
            {
                SelectedText = "Startvalue y",
                Tag = "temp",

            };
            lineoptions.Children.Add(starty);

            TextBox endx = new TextBox
            {
                SelectedText = "Endvalue x",
                Tag = "temp",
            };
            lineoptions.Children.Add(endx);

            TextBox endy = new TextBox
            {
                SelectedText = "Endvalue y",
                Tag = "temp",

            };
            lineoptions.Children.Add(endy);

            Button draw = new Button();
            draw.Click += new RoutedEventHandler(Drawline);
            draw.Height = 20;
            draw.Content = "Zeichne";
            draw.Tag = "temp";
            lineoptions.Children.Add(draw);

            LinePanel.Children.Add(lineoptions);

        }
        private void Drawline(object sender, EventArgs e)
        {
            BTLin.IsEnabled = true;
            Line l = new Line();
            TextBox t = (TextBox)lineoptions.Children[0];
            l.X1 = Convert.ToInt32(t.Text);
            t = (TextBox)lineoptions.Children[1];
            l.Y1 = Convert.ToInt32(t.Text);
            t = (TextBox)lineoptions.Children[2];
            l.X2 = Convert.ToInt32(t.Text);
            t = (TextBox)lineoptions.Children[3];
            l.Y2 = Convert.ToInt32(t.Text);
            MyLine ML = new MyLine(l);
            ML.draw(Drawingspace);
            LinePanel.Children.Remove(lineoptions);

        }

        private void BTCric_Click(object sender, RoutedEventArgs e)
        {
            BTCric.IsEnabled = false;
            circoptions.Children.RemoveRange(0, circoptions.Children.Count);
            TextBox radius = new TextBox
            {
                SelectedText = "Radius",
                Tag = "temp",

            };
            circoptions.Children.Add(radius);

            TextBox cx = new TextBox
            {
                SelectedText = "Center X",
                Tag = "temp",

            };
            circoptions.Children.Add(cx);

            TextBox cy = new TextBox
            {
                SelectedText = "Center Y",
                Tag = "temp",
            };
            circoptions.Children.Add(cy);

            Button draw = new Button();
            draw.Click += new RoutedEventHandler(Drawcirc);
            draw.Height = 20;
            draw.Content = "Zeichne";
            draw.Tag = "temp";
            circoptions.Children.Add(draw);

            CirclePanel.Children.Add(circoptions);
        }
        private void Drawcirc(object sender, EventArgs e)
        {
            BTCric.IsEnabled = true;
            Ellipse el = new Ellipse();
            int cx = 0, cy = 0;

            TextBox t = (TextBox)circoptions.Children[0];
            el.Width = Convert.ToInt32(t.Text) * 2;
            el.Height = Convert.ToInt32(t.Text) * 2;
            t = (TextBox)circoptions.Children[1];
            cx = Convert.ToInt32(t.Text);
            t = (TextBox)circoptions.Children[2];
            cy = Convert.ToInt32(t.Text);
            MyCircle MC = new MyCircle(el, cx, cy);
            MC.draw(Drawingspace);


            CirclePanel.Children.Remove(circoptions);
        }

        private void BTRect_Click(object sender, RoutedEventArgs e)
        {
            {
                BTRect.IsEnabled = false;
                rectoptions.Children.RemoveRange(0, rectoptions.Children.Count);
                TextBox startx = new TextBox
                {
                    SelectedText = "TopLeftValue x",
                    Tag = "temp",

                };
                rectoptions.Children.Add(startx);

                TextBox starty = new TextBox
                {
                    SelectedText = "TopLeftvalue y",
                    Tag = "temp",

                };
                rectoptions.Children.Add(starty);

                TextBox heigth = new TextBox
                {
                    SelectedText = "Height",
                    Tag = "temp",
                };
                rectoptions.Children.Add(heigth);

                TextBox width = new TextBox
                {
                    SelectedText = "Width",
                    Tag = "temp",

                };
                rectoptions.Children.Add(width);

                Button draw = new Button();
                draw.Click += new RoutedEventHandler(Drawrect);
                draw.Height = 20;
                draw.Content = "Zeichne";
                draw.Tag = "temp";
                rectoptions.Children.Add(draw);

                RectanglePanel.Children.Add(rectoptions);
            }
        }
        private void Drawrect(object sender, EventArgs e)
        {
            BTRect.IsEnabled = true;
            int x = 0, y = 0;
            Rectangle r = new Rectangle();
            TextBox t = (TextBox)rectoptions.Children[0];
            x = Convert.ToInt32(t.Text);
            t = (TextBox)rectoptions.Children[1];
            y = Convert.ToInt32(t.Text);
            t = (TextBox)rectoptions.Children[2];
            r.Height = Convert.ToInt32(t.Text);
            t = (TextBox)rectoptions.Children[3];
            r.Width = Convert.ToInt32(t.Text);
            MyRectangle MR = new MyRectangle(r, x, y);
            MR.draw(Drawingspace);
            RectanglePanel.Children.Remove(rectoptions);
        }

    }
}

