﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Controls;



namespace CAD_Decorator
{
    class MyRectangle:AShape
    {
        Rectangle tempR = new Rectangle();
        public override Shape s
        {
            get { return tempR; }
        }
        public override bool IsSelected
        {
            get { return IsSelected; }
            set { IsSelected = false; }
        }
        public MyRectangle(Rectangle s,int x,int y):base(s)
        {
            this.tempR = new Rectangle {Height=s.Height,Width=s.Width, Stroke = System.Windows.Media.Brushes.Black, StrokeThickness = 3 };
            Canvas.SetTop(tempR, y);
            Canvas.SetLeft(tempR, x);
        }

    }
}
