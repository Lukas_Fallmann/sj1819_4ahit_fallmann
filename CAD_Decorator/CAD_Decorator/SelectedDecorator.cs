﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD_Decorator
{
    class SelectedDecorator:AShapeDecorator
    {
        public SelectedDecorator(AShape Form):base(Form)
        {
            //Form.IsSelected = true;
            Form.s.Stroke = System.Windows.Media.Brushes.Blue;
        }
    }
}
