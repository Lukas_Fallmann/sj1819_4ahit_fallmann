﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Shapes;


namespace CAD_Decorator
{
    class MyLine : AShape
    {

        Line tempL = new Line();
        public override Shape s
        {
            get { return tempL; }
        }
        public override bool IsSelected
        {
            get { return IsSelected; }
            set { IsSelected = false; }
        }
        public MyLine(Line s):base(s)
        {
            this.tempL = new Line { X1 = s.X1, X2 = s.X2, Y1 = s.Y1, Y2 = s.Y2, Stroke = System.Windows.Media.Brushes.Black, StrokeThickness = 3 };
            //this.tempL.MouseLeftButtonDown += S_MouseLeftButtonDown;
            this.tempL.MouseDown += new MouseButtonEventHandler(S_MouseLeftButtonDown);
        }
        public void S_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            AComponent selected = new SelectedDecorator(this);

        }


    }

}

