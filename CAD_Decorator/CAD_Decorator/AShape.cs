﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace CAD_Decorator
{
    abstract class AShape:AComponent
    {
        abstract public bool IsSelected { get; set; }
        List<AComponent> componenten = new List<AComponent>();
        
        public AShape(Shape s)
        {
            componenten.Add(this);
        }

    }
}
