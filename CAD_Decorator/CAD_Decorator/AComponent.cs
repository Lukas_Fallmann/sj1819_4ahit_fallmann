﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace CAD_Decorator
{
    abstract class AComponent
    {
        public virtual Shape s { get; set; }

        public void draw(Canvas drawingspace)
        {
            drawingspace.Children.Add(s);
        }

    }
}
