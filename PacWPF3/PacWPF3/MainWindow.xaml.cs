﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PacWPF3
{
    enum Direction { Up, Down, Left, Right };
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer t = new DispatcherTimer();
        DispatcherTimer t1 = new DispatcherTimer();
        Game g;
        int bigPillC;
        int PillC;
        Random rnd = new Random();
        public MainWindow()
        {
            InitializeComponent();
           
            window.Loaded += Window_Loaded;
            KeyDown += MainWindow_KeyDown;
            t.Interval = TimeSpan.FromMilliseconds(100);
            t.Tick += CheckList;
            bigPillC = 0;
            PillC = 0;
            t1.Interval = TimeSpan.FromMilliseconds(100);
            t1.Tick += T1_Tick;


        }

        private void T1_Tick(object sender, EventArgs e)
        {
            Color rndcolor = Color.FromRgb((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            Center.Background = new SolidColorBrush(rndcolor);
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    g.p.Turn(Direction.Up);
                    break;
                case Key.Down:
                    g.p.Turn(Direction.Down);
                    break;
                case Key.Left:
                    g.p.Turn(Direction.Left);
                    break;
                case Key.Right:
                    g.p.Turn(Direction.Right);
                    break;
                case Key.Space:
                    t.Tick += g.p.MoveOn;
                    t.Start();
                    break;
            }
        }
       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            g = new Game(50, Center);
            foreach (Pill p in g.Pills)
                if (p is BigPill)
                    bigPillC++;
            PillC = g.Pills.Count - bigPillC;

        }
        public void CheckList(object sender, EventArgs e)
        {
            int valueB = 0;
            int valueP = 0;
            int tempBigP = 0;
            int tempP = 0;
            foreach (Pill p in g.Pills)
            {
                if (p is BigPill)
                {
                    valueB = p.Value;
                    tempBigP++;
                }
                else valueP = p.Value;
            }
            tempP = g.Pills.Count - tempBigP;
            if (tempP < PillC)
            {
                PillC = tempP;
                lcounter.Content = Convert.ToInt32(lcounter.Content) + valueP;
            }
            if (tempBigP < bigPillC)
            {
                bigPillC = tempBigP;
                lcounter.Content = Convert.ToInt32(lcounter.Content) + valueB;
            }

            if (g.Pills.Count <= 1)
            {
                Center.Children.RemoveRange(0, Center.Children.Count);
                Label l = new Label();
                l.Content = "Game over";
                l.FontSize = 50;
                l.Width = 250;
                l.Foreground = new SolidColorBrush(Colors.Purple);
                Center.Background = new SolidColorBrush(Colors.LightGoldenrodYellow);
                Center.Children.Add(l);
                Canvas.SetTop(l, Center.ActualHeight / 2);
                Canvas.SetLeft(l, Center.ActualWidth / 2-l.Width/2);
                t.Stop();
                t1.Start();


            }

        }
    }
}
