﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PacWPF3
{
    class BigPill : Pill
    {
        public override Rectangle R { get => base.R; set => base.R = value; }
        public override int Value { get => base.Value; set => base.Value = value; }
        public override int[] Location { get => base.Location; set => base.Location = value; }
        public BigPill()
        {
            this.Value = 300;
            this.R = new Rectangle { Height = 15, Width = 15, Fill = new SolidColorBrush(Colors.Red) };
            this.Location = new int[2];
        }
        
    }
}
