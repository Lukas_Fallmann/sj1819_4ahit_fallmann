﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfAnimatedGif;

namespace PacWPF3
{
    class Pacman
    {
        public Image myimage = new Image();
        int cellwidthheight = 0;
        Canvas c;
        int xPos, yPos, stepwidth;
        protected Direction currentDirection = Direction.Right;
        Direction newD = Direction.Right;
        List<Pill> Pills;
        public Pacman(int stepwidth, int cellwidth, Canvas c, List<Pill> pills)
        {
            this.Pills = pills;
            myimage.RenderTransformOrigin = new Point(0.5, 0.5);
            myimage.Loaded += Myimage_Loaded;
            c.Children.Add(myimage);
            this.c = c;
            this.stepwidth = stepwidth;
            this.cellwidthheight = cellwidth;
            TranslateTransform setStartPoint = new TranslateTransform(cellwidthheight - 15, cellwidthheight - 15);
            myimage.RenderTransform = setStartPoint;
            xPos = cellwidthheight;
            yPos = cellwidthheight;

        }

        private void Myimage_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage theImage = new BitmapImage();
            theImage.BeginInit();
            theImage.UriSource = (new Uri(@"C:\Users\lukas\Documents\Schule\SEW\sj1819_4ahit_fallmann\PacPrototype1\PacPrototype1\bin\Debug\pacman.gif"));
            theImage.EndInit();

            var image = sender as Image;

            ImageBehavior.SetAnimatedSource(image, theImage);
            image.Width = 30;
            image.Height = 30;
        }

        public void Turn(Direction newDirection)
        {
            this.newD = newDirection;
            if ((currentDirection == Direction.Down || currentDirection == Direction.Up) && (newDirection == Direction.Down || newDirection == Direction.Up))
                this.currentDirection = newDirection;
            if ((currentDirection == Direction.Left || currentDirection == Direction.Right) && (newDirection == Direction.Left || newDirection == Direction.Right))
                this.currentDirection = newDirection;
        }
        public void MoveOn(object sender, EventArgs e)
        {
            if (currentDirection != newD)
            {
                if ((xPos % cellwidthheight == 0) && (yPos % cellwidthheight == 0))
                    currentDirection = newD;
            }
            if (xPos == 0 || xPos == c.ActualWidth || yPos == 0 || yPos == c.ActualHeight)
            {
                switch (currentDirection)
                {
                    case Direction.Up:
                        currentDirection = Direction.Down;
                        newD = Direction.Down;
                        break;
                    case Direction.Down:
                        currentDirection = Direction.Up;
                        newD = Direction.Up;
                        break;
                    case Direction.Left:
                        currentDirection = Direction.Right;
                        newD = Direction.Right;
                        break;
                    case Direction.Right:
                        currentDirection = Direction.Left;
                        newD = Direction.Left;
                        break;
                }
            }
            TransformGroup t = new TransformGroup();
            switch (currentDirection)
            {
                case Direction.Up:
                    RotateTransform up = new RotateTransform(-90);
                    TranslateTransform moveup = new TranslateTransform(xPos - 15, yPos -= stepwidth);
                    t.Children.Add(up);
                    t.Children.Add(moveup);
                    myimage.RenderTransform = t;
                    break;
                case Direction.Down:
                    RotateTransform down = new RotateTransform(90);
                    TranslateTransform movedown = new TranslateTransform(xPos - 15, yPos += stepwidth);
                    t.Children.Add(down);
                    t.Children.Add(movedown);
                    myimage.RenderTransform = t;
                    break;
                case Direction.Left:
                    RotateTransform left = new RotateTransform(180);
                    TranslateTransform moveleft = new TranslateTransform(xPos -= stepwidth, yPos - 15);
                    t.Children.Add(left);
                    t.Children.Add(moveleft);
                    myimage.RenderTransform = t;
                    break;
                case Direction.Right:
                    RotateTransform right = new RotateTransform(0);
                    TranslateTransform moveright = new TranslateTransform(xPos += stepwidth, yPos - 15);
                    t.Children.Add(right);
                    t.Children.Add(moveright);
                    myimage.RenderTransform = t;
                    break;

            }
            Checkjunction();
        }

        public void Checkjunction()
        {
            int amount = Pills.Count();
            if ((xPos % cellwidthheight == 0) && (yPos % cellwidthheight == 0))
                for (int i = 0; i <= amount-2; i++)
                {
                    if (xPos == Pills[i].Location[0] && yPos == Pills[i].Location[1])
                    {
                        c.Children.Remove(Pills[i].R);
                        Pills.Remove(Pills[i]);
                       
                    }
                }
        }


    }
}
