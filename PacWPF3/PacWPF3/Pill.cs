﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PacWPF3
{
    class Pill
    {
        public virtual int Value { get; set; }
        public virtual Rectangle R { get; set; }
        public virtual int[] Location { get; set; }
        public Pill(){
            this.Value = 150;
            this.R = new Rectangle { Height = 10, Width = 10, Fill = new SolidColorBrush(Colors.Blue) };
            this.Location = new int[2];

        }
    }
}
