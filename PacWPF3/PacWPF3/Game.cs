﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PacWPF3
{
    class Game
    {
        public Pacman p { get; set; }
        int cell;
        Canvas c;
        public List<Pill> Pills { get; set; }
        Random random = new Random();
        public Game(int cell,Canvas c)
        {
            Pills = new List<Pill>();
            double remainingX = c.ActualWidth % cell;
            double remainingY = c.ActualHeight % cell;
            int gridcellsvert = Convert.ToInt32(c.ActualHeight / cell);
            int gridcellshori = Convert.ToInt32(c.ActualWidth / cell);

            Canvas gamefield = new Canvas();
            gamefield.Width = c.ActualWidth - remainingX;
            gamefield.Height = c.ActualHeight - remainingY;
            gamefield.Background = System.Windows.Media.Brushes.LightSteelBlue;

            for (int i = 0; i <= gridcellshori-1; i++)
            {
                Line l = new Line();
                l.X1 = i * cell;
                l.X2 = i * cell;
                l.Y1 = 0;
                l.Y2 = gamefield.Height;
                l.Stroke = System.Windows.Media.Brushes.Black;
                l.StrokeThickness = 3;
                gamefield.Children.Add(l);
            }

            for (int i = 0; i <= gridcellsvert; i++)
            {
                Line l = new Line();
                l.X1 = 0;
                l.X2 = gamefield.Width;
                l.Y1 = i*cell;
                l.Y2 = i*cell;
                l.Stroke = System.Windows.Media.Brushes.Black;
                l.StrokeThickness = 3;
                gamefield.Children.Add(l);
            }
            for (int i = 1; i < gridcellsvert; i++)
            {
                for (int j = 1; j <= gridcellshori-2; j++)
                {
                    if (!(i == 1 && j == 1))
                    {
                        Pill p = new Pill();
                        bool bigpill = Convert.ToBoolean(random.Next(0, 2));
                        if (bigpill)
                            p = new BigPill();
                        gamefield.Children.Add(p.R);
                        Canvas.SetTop(p.R, i * 50 - p.R.Width / 2);
                        Canvas.SetLeft(p.R, j * 50 - p.R.Width / 2);
                        p.Location[0] = j * 50;
                        p.Location[1] = i * 50;
                        Pills.Add(p);
                    }
                }
            }
            this.c = c;
            this.cell = cell;
            c.Children.Add(gamefield);
            Canvas.SetTop(gamefield, remainingY / 2);
            Canvas.SetLeft(gamefield, remainingX / 2);
            gamefield.Loaded += Gamefield_Loaded;
            
        }

        private void Gamefield_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var gamefield = sender as Canvas;
            p = new Pacman(5, cell, gamefield,Pills);
        }
    }
}
