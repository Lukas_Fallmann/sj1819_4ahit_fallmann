﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackgroundWorker
{
    public partial class Form1 : Form
    {
        private string file = "Test.txt";
        public Form1()
        {
            InitializeComponent();
            // Datei erzeugen und mit Zeichen füllen      
            FileStream fs = File.Open(file, FileMode.Create);
            byte b = 97;      for (int i = 0; i < 20000; i++)
            {
                fs.WriteByte(b++);
                if (b == 123) b = 97;
            }      fs.Close();
        }  
        


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int var = 0;
            string text = "";
            // Datei öffnen
            FileStream fs = File.Open(e.Argument.ToString(), FileMode.Open);
            FileInfo fi = new FileInfo(file);
            // Dateigröße
            int fileLength = (int)new FileInfo(file).Length;
            int counter = 0;
            while (true)
            {
                var = fs.ReadByte();
                // wenn Datei eingelesen, Schleife beenden
                if (var==-1)  
                    break;
                text += Convert.ToChar(var);
                // Das Ereignis 'ProgressChanged' auslösen
                counter++;
                this.backgroundWorker1.ReportProgress(counter * 100 / fileLength);
            }
            e.Result = text;
            fs.Close();   
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.Refresh();
            // ProgressBar einstellen
            proFuellen.Maximum = 100;
            proFuellen.Step = 1;
            proFuellen.Value = 0;
            // BackGroundWorker starten
            this.backgroundWorker1.RunWorkerAsync(file);  
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            File.Delete(file);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.textBox1.Text = Convert.ToString(e.Result);
            this.proFuellen.Value = 100;
        }

        private void backgroundWorker1_ProgressChanged_1(object sender, ProgressChangedEventArgs e)
        {
                // ProgressBar einstellen
                proFuellen.Value = e.ProgressPercentage;
            
        }
    }
}
