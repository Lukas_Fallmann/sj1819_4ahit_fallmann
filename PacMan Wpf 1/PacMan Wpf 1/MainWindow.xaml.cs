﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PacMan_Wpf_1
{
    enum Direction { Up, Down, Left, Right };
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        DispatcherTimer t = new DispatcherTimer();
        
        
        Pacman p;
        public MainWindow()
        {
            InitializeComponent();
            window.Loaded += Window_Loaded;
            KeyDown += MainWindow_KeyDown;
            t.Interval = TimeSpan.FromMilliseconds(150);
            

        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    p.Turn(Direction.Up);
                    break;
                case Key.Down:
                    p.Turn(Direction.Down);
                    break;
                case Key.Left:
                    p.Turn(Direction.Left);
                    break;
                case Key.Right:
                    p.Turn(Direction.Right);
                    break;
                case Key.Space:
                    t.Start();
                    break;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Center.Width = window.Width;
            Center.Height = window.Height - 60;
            Game g = new Game(50, Center);
            p = new Pacman(25, (Grid)Center.Children[0]);
            t.Tick += p.MoveOn;
        }
    }
}
