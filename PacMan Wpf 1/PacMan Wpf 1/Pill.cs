﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PacMan_Wpf_1
{
    class Pill
    {
        public Rectangle r { get; set; }
        public Pill()
        {
            this.r = new Rectangle { Height=20,Width=20,Fill= new SolidColorBrush(Colors.Blue)};
        }
    }
}
