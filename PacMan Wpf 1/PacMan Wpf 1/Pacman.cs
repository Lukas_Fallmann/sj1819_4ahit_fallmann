﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace PacMan_Wpf_1
{
    class Pacman
    {
        public Image myimage = new Image();
        int cellwidthheight = 0;
        Grid g;
        int xPos, yPos,stepwidth;
        protected Direction currentDirection=Direction.Right;
        public Pacman (int stepwidth,Grid g)
        {
            myimage.RenderTransformOrigin = new Point(0.5, 0.5);
            myimage.Loaded += Myimage_Loaded;
            g.Children.Add(myimage);
            g.Loaded += G_Loaded;
            this.g = g;
            this.stepwidth = stepwidth;
            
        }

        private void G_Loaded(object sender, RoutedEventArgs e)
        {
            var g = sender as Grid;
            cellwidthheight = Convert.ToInt32(g.ColumnDefinitions[1].ActualWidth);
            TranslateTransform setStartPoint = new TranslateTransform(cellwidthheight/2, cellwidthheight/2);
            myimage.RenderTransform = setStartPoint;
            xPos = cellwidthheight / 2;
            yPos = cellwidthheight / 2;
            
        }

        private void Myimage_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage theImage = new BitmapImage();
            theImage.BeginInit();
            theImage.UriSource = (new Uri(@"C:\Users\lukas\Documents\Schule\SEW\sj1819_4ahit_fallmann\PacPrototype1\PacPrototype1\bin\Debug\pacman.gif"));
            theImage.EndInit();

            var image = sender as Image;

            ImageBehavior.SetAnimatedSource(image, theImage);
            image.Width = 30;
            image.Height = 30;
        }
        
        public void Turn(Direction newDirection)
        {
            int gheight = Convert.ToInt32(g.ActualHeight);
            int gWidth = Convert.ToInt32(g.ActualWidth);
            if (gheight % (yPos+ cellwidthheight/2) <=cellwidthheight && gWidth % (xPos+cellwidthheight/2) <= cellwidthheight)
                this.currentDirection = newDirection;
            if ((currentDirection == Direction.Down || currentDirection == Direction.Up) && (newDirection == Direction.Down || newDirection == Direction.Up))
                this.currentDirection = newDirection;
            if ((currentDirection == Direction.Left || currentDirection == Direction.Right) && (newDirection == Direction.Left || newDirection == Direction.Right))
                this.currentDirection = newDirection;
        }
        public void MoveOn(object sender, EventArgs e)
        {
            TransformGroup t = new TransformGroup();
            switch (currentDirection)
            {
                case Direction.Up:
                    RotateTransform up = new RotateTransform(-90);
                    TranslateTransform moveup = new TranslateTransform(xPos, yPos-=stepwidth);
                    t.Children.Add(up);
                    t.Children.Add(moveup);
                    myimage.RenderTransform = t;
                    break;
                case Direction.Down:
                    RotateTransform down = new RotateTransform(90);
                    TranslateTransform movedown = new TranslateTransform(xPos,yPos+=stepwidth );
                    t.Children.Add(down);
                    t.Children.Add(movedown);
                    myimage.RenderTransform = t;
                    break;
                case Direction.Left:
                    RotateTransform left = new RotateTransform(180);
                    TranslateTransform moveleft = new TranslateTransform(xPos -= stepwidth, yPos);
                    t.Children.Add(left);
                    t.Children.Add(moveleft);
                    myimage.RenderTransform = t;
                    break;
                case Direction.Right:
                    RotateTransform right = new RotateTransform(0);
                    TranslateTransform moveright = new TranslateTransform(xPos+=stepwidth, yPos);
                    t.Children.Add(right);
                    t.Children.Add(moveright);
                    myimage.RenderTransform = t;
                    break;
 
            }
        }


    }
    }
