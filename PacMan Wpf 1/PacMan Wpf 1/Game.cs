﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace PacMan_Wpf_1
{
    class Game
    {
        public Game(int cellwidth, Canvas c)
        {
            double remainingX = c.ActualWidth %  cellwidth;
            double remainingY = c.ActualHeight % cellwidth;
            int gridcellsvert = Convert.ToInt32(c.ActualHeight / cellwidth);
            int gridcellshori = Convert.ToInt32(c.ActualWidth / cellwidth) ;

            //Grid creation
            Grid gamefield = new Grid();
            gamefield.Width = c.ActualWidth - remainingX;
            gamefield.Height = c.ActualHeight - remainingY;
            gamefield.ShowGridLines = true;
            gamefield.Background = new SolidColorBrush(Colors.SteelBlue);

            //Rows&Collums
            for (int i = 0; i < gridcellshori; i++)
            {
                ColumnDefinition cD = new ColumnDefinition();
                cD.Width = new System.Windows.GridLength(cellwidth);
                gamefield.ColumnDefinitions.Add(cD);
            }
            for (int i = 0; i < gridcellsvert; i++)
            {
                RowDefinition rD = new RowDefinition();
                rD.Height = new System.Windows.GridLength(cellwidth);
                gamefield.RowDefinitions.Add(rD);
            }

            //Adding Pills
            for (int i = 0; i < gridcellsvert; i++)
            {
                for (int j = 0; j < gridcellshori; j++)
                {
                    Pill p = new Pill();
                    gamefield.Children.Add(p.r);
                }
            }

            c.Children.Add(gamefield);
            Canvas.SetTop(gamefield, remainingY / 2);
            Canvas.SetLeft(gamefield, remainingX / 2);
        }
    }
}
