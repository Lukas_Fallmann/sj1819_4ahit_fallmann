﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Composite_Pattern
{
    abstract class AGrafik
    {
        protected Control c;
        protected Color col;
        protected int left;
        protected int top;
        public virtual void Draw(int level) { }

    }
   
}
