﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Text_Ticker;

namespace GraphicText_Ticker
{
    public partial class Form1 : Form
    {
        TextTicker t;
        public Form1()
        {
            Draw();
            InitializeComponent();
            t = new TextTicker("iloveit");
        }
        
        public void Bt_Shiftleft_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            timer1.Start();
            timer1.Tick += new EventHandler(timer1_Tick);

        }
        private void Draw()
        {

            float length = 17;
            float height = 23;
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 33; j++)
                {
                    Panel panel = new Panel();
                    panel.Width = Convert.ToInt32(length);
                    panel.Height = Convert.ToInt32(height);
                    panel.Top = Convert.ToInt32(i * height + 2);
                    panel.Left = Convert.ToInt32(j * length + 18);
                    panel.BackColor = Color.LightGray;
                    panel.Name = Convert.ToString(i + "" + j);
                    panel.BorderStyle = BorderStyle.FixedSingle;

                    Controls.Add(panel);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            t.ShiftLeft();
            int i = 0, j = 0;
            foreach (Control c in Controls)
            {

                if (c is Panel)
                {
                    if (t.WholeText[i, j])
                        c.BackColor = Color.Blue;
                    else
                        c.BackColor = Color.LightGray;
                    j++;
                    if (j == 33)
                    {
                        i++;
                        j = 0;
                    }
                }
               

            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            t.ShiftRight();
            int i = 0, j = 0;
            foreach (Control c in Controls)
            {

                if (c is Panel)
                {
                    if (t.WholeText[i, j])
                        c.BackColor = Color.Blue;
                    else
                        c.BackColor = Color.LightGray;
                    j++;
                    if (j == 33)
                    {
                        i++;
                        j = 0;
                    }
                }
               

            }
        }

        private void Bt_Shiftright_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            timer2.Start();
            timer2.Tick += new EventHandler(timer2_Tick);
        }
    }
}
