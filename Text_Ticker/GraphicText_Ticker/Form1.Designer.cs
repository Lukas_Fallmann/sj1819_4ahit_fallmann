﻿namespace GraphicText_Ticker
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Bt_Shiftleft = new System.Windows.Forms.Button();
            this.Bt_Shiftright = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Bt_Shiftleft
            // 
            this.Bt_Shiftleft.Location = new System.Drawing.Point(222, 623);
            this.Bt_Shiftleft.Name = "Bt_Shiftleft";
            this.Bt_Shiftleft.Size = new System.Drawing.Size(182, 41);
            this.Bt_Shiftleft.TabIndex = 1;
            this.Bt_Shiftleft.Text = "Shiftleft";
            this.Bt_Shiftleft.UseVisualStyleBackColor = true;
            this.Bt_Shiftleft.Click += new System.EventHandler(this.Bt_Shiftleft_Click);
            // 
            // Bt_Shiftright
            // 
            this.Bt_Shiftright.Location = new System.Drawing.Point(1005, 617);
            this.Bt_Shiftright.Name = "Bt_Shiftright";
            this.Bt_Shiftright.Size = new System.Drawing.Size(182, 41);
            this.Bt_Shiftright.TabIndex = 2;
            this.Bt_Shiftright.Text = "Shiftright";
            this.Bt_Shiftright.UseVisualStyleBackColor = true;
            this.Bt_Shiftright.Click += new System.EventHandler(this.Bt_Shiftright_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1438, 816);
            this.Controls.Add(this.Bt_Shiftright);
            this.Controls.Add(this.Bt_Shiftleft);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button Bt_Shiftleft;
        private System.Windows.Forms.Button Bt_Shiftright;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
    }
}

