﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text_Ticker
{
    class TextTicker
    {
        public bool[,] WholeText
        {
            get { return wholeText; }
            set { WholeText = value; }
        }

        bool[,] wholeText = new bool[7, 33];

        public TextTicker(string input)
        {
            int position = 0;

            if (input.Length < 8)
            {
                int remaining = 8 - input.Length;
                for (int i = 0; i < remaining; i++)
                {
                    input += " ";
                }
            }
            foreach (char c in input.ToUpper())
            {
                bool[,] singleLetters = fieldLetters[c];

                for (int i = 0; i < singleLetters.GetLength(0); i++)
                {
                    for (int j = 0; j < singleLetters.GetLength(1); j++)
                    {
                        wholeText[i, j + position] = singleLetters[i, j];
                    }
                }

                position += 4;
            }

        }
        public void ShiftLeft()
        {
            bool[] help = new bool[7];
            for (int i = 0; i < 7; i++)
            {
                help[i] = WholeText[i, 0];
            }

            for (int i = 1; i < WholeText.GetLength(1); i++)
            {
                for (int j = 0; j < WholeText.GetLength(0); j++)
                {
                    WholeText[j, i - 1] = wholeText[j, i];
                }
            }
            for (int i = 0; i < 7; i++)
            {
                WholeText[i, 32] = help[i];
            }

        }
        public void ShiftRight()
        {

            bool[] help = new bool[7];
            for (int i = 0; i < 7; i++)
            {
                help[i] = WholeText[i, 32];
            }

            for (int i = 31; i > 0; i--)
            {
                for (int j = 0; j < WholeText.GetLength(0); j++)
                {
                    wholeText[j, i + 1] = WholeText[j, i];
                }
            }
            for (int i = 0; i < 7; i++)
            {
                WholeText[i, 1] = help[i];
            }

        }

        public void showArray()
        {
            Console.Clear();
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 33; j++)
                {
                    if (WholeText[i, j])
                        Console.Write('%');
                    else Console.Write(' ');
                }
                Console.WriteLine();
            }
        }

        Dictionary<char, bool[,]> fieldLetters = new Dictionary<char, bool[,]> {

                { 'I', new bool[7,3]{{false,false,false },{false,true,false },{false,true,false },{false,true,false },{false,true,false },{false,true,false },{false,false,false } } },
                { 'L', new bool[7,3]{ {false,false,false},{true,false,false },{true,false,false },{ true,false,false},{ true,false,false},{ true,true,true},{ false,false,false} } },
                { 'O', new bool[7,3]{ {false,false,false},{false,true,false },{true,false,true },{ true,false,true},{ true,false,true},{ false,true,false},{ false,false,false} } },
                { 'V', new bool[7,3]{ {false,false,false},{true,false,true },{true,false,true},{true,false,true},{true,false,true },{false,true,false},{false,false,false} } },
                { 'E', new bool[7,3]{ {false,false,false},{true,true,true },{true,false,false},{true,true,false },{true,false,false },{true,true,true},{false,false,false} } },
                { 'T', new bool[7,3]{ {false,false,false},{true,true,true },{false,true,false},{false,true,false },{false,true,false },{false,true,false},{false,false,false} } },
                { ' ', new bool[7,3]{ {false,false,false},{false,false,false },{false,false,false},{false,false,false },{false,false,false },{false,false,false},{false,false,false} } },
        };
    }
}
