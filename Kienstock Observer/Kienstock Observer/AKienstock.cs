﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kienstock_Observer
{
    abstract class AKienstock
    {
        List<AObserver> subscribers = new List<AObserver>();
        public void Attach(AObserver sub)
        { subscribers.Add(sub); }
        public void Detach(AObserver sub)
        { subscribers.Remove(sub); }

        public void Notify()
        {
            foreach (AObserver sub in subscribers)
            {
                sub.Update();
            }
        }
    }
}
