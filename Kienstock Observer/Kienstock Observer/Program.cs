﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kienstock_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Kienstock k = new Kienstock();
            Katastrophenschutz ks = new Katastrophenschutz(k);
            Schifffahrtsbehörde s = new Schifffahrtsbehörde(k);
            Landeswarnzentrale l = new Landeswarnzentrale(k);
            k.Attach(ks);
            k.Attach(s);
            k.Attach(l);

            k.Wasserstand = 50;
            Console.WriteLine(k.Wasserstand);
            k.Notify();

            Console.ReadKey();

            k.Wasserstand = 60;
            Console.WriteLine(k.Wasserstand);
            k.Notify();

            Console.ReadKey();

            k.Wasserstand = 80;
            Console.WriteLine(k.Wasserstand);
            k.Notify();

            Console.ReadKey();

            k.Wasserstand = 10;
            Console.WriteLine(k.Wasserstand);
            k.Notify();

            Console.ReadKey();
        }
    }
}
