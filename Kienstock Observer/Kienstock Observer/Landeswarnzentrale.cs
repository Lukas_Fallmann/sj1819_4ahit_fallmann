﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kienstock_Observer
{
    class Landeswarnzentrale : AObserver
    {
        Kienstock subscribable = new Kienstock();
        public double Wasserstand { get; set; }
        public Landeswarnzentrale(AKienstock k)
        {
            subscribable = (Kienstock)k;

            Wasserstand = this.subscribable.GetState();

        }

        public override void Update()
        {
            Wasserstand = this.subscribable.GetState();
            if (Wasserstand > 70)
                Console.WriteLine("Landeswarnzentrale hat ein Problem");
        }
    }
}
