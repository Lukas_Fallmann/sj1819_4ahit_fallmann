﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kienstock_Observer
{
    class Kienstock: AKienstock
    {
        public double Wasserstand { get; set; }
        public Kienstock()
        {
            this.Wasserstand = 20;
        }
        public double GetState()
        {
            return Wasserstand;
        }
    }
}
