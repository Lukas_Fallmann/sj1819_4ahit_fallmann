﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XML_Start
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<root><Note>Hugo</Note></root>");
            XmlElement n = doc.CreateElement("Name");
            n.InnerText = "Franz";
            XmlNode r = doc.SelectSingleNode("root");
            r.AppendChild(n);

            XmlAttribute a = doc.CreateAttribute("id");
            a.Value = "12";
            n.Attributes.Append(a);

            doc.Save("test.xml");

        }
    }
}
