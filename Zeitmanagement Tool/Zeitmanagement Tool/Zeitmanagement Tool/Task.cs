﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zeitmanagement_Tool
{
    public class Task
    {
        public bool Urgent { get; set; }
        public bool Important { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Done { get; set; }
        public State State { get; set; }

        public Task(string title, string description, bool important, bool urgent)
        {
            this.Description = description;
            this.Title = title;
            this.Important = important;
            this.Urgent = urgent;
            this.State = GetState(important, urgent);
        }

        private State GetState(bool important, bool urgent)
        {
            if (important && urgent) return State.A;
            if (important && !urgent) return State.B;
            if (!important && urgent) return State.C;
            return State.D;
        }
    }
}