﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;


namespace TimetoolDLL
{
    public class TimeManagement
    {
        private string xmlPath;

        public List<Task> Tasks { get; set; }
        public XDocument Doc { get; set; }
        public TimeManagement(string xmlPath)
        {
            this.xmlPath = xmlPath;
            Tasks = new List<Task>();
            Doc = XDocument.Load(xmlPath);
            LoadTasks(xmlPath);
        }

        public bool AddTask(Task t)
        {
            Tasks.Add(t);
            try
            {
                //XML Logic

                XElement Task = new XElement("Task",
                new XAttribute("ID", Tasks.IndexOf(t)),
                new XElement("Title", t.Title),
                new XElement("Description", t.Description),
                new XElement("Important", Convert.ToString(t.Important)),
                new XElement("Urgent", Convert.ToString(t.Urgent)),
                new XElement("Done", Convert.ToString(t.Done)));

                XElement node = Doc.Element("Tasks");
                node.Add(Task);
                Doc.Save(xmlPath);
                return true;
            }
            catch (Exception)
            { return false; }

        }

        public bool LoadTasks(string xmlPath)
        {
            try
            {
                //XML Logic
                var items = from i in XDocument.Load(xmlPath).Descendants("Tasks").Descendants("Task")
                            select new Task(i.Element("Title").Value,
                                 i.Element("Description").Value,
                                 Convert.ToBoolean(i.Element("Important").Value),
                                 Convert.ToBoolean(i.Element("Urgent").Value));
                foreach (Task task in items)
                {
                    Tasks.Add(task);
                }
                return true;
            }
            catch (Exception) { return false; }

        }

        public List<Task> GetTasks(State Classification)
        {
            List<Task> tempTasks = new List<Task>();
            foreach (Task task in Tasks)
            {
                if (task.State == Classification)
                {
                    tempTasks.Add(task);
                }
            }
            return tempTasks;
        }
    }
}