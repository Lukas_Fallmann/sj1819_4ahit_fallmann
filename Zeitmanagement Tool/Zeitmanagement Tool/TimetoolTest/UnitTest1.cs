﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimetoolDLL;

namespace TimetoolTest
{
    [TestClass]
    public class UnitTest1
    {
       public TimeManagement time = new TimeManagement("time.xml");

        [TestMethod]
        public void LoadTest()
        {
            List<Task> tasks = new List<Task>();
            Assert.AreEqual(true, time.LoadTasks("time.xml"));
        }
        [TestMethod]
        public void SaveTask()
        {
            Task t = new Task("Herbert", "Herbert hänseln", true, true);
            Assert.AreEqual(true, time.AddTask(t));
        }

        [TestMethod]
        public void GetStateTest()
        {
            Task t = new Task("Herbert", "Herbert hänseln", true, true);
            Assert.AreEqual(State.A, t.State);
        }

        [TestMethod]

        public void GetTasks()
        {
            List<Task> t = new List<Task>();
            Assert.AreEqual(t.Count, time.GetTasks(State.C).Count);
        }
    }
}
