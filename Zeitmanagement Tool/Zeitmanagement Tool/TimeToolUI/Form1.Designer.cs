﻿namespace TimeToolUI
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.DetailPanel = new System.Windows.Forms.Panel();
            this.SaveBT = new System.Windows.Forms.Button();
            this.DescriptionTB = new System.Windows.Forms.TextBox();
            this.TitleTB = new System.Windows.Forms.TextBox();
            this.Done = new System.Windows.Forms.CheckBox();
            this.CancelBT = new System.Windows.Forms.Button();
            this.CreateNewTask = new System.Windows.Forms.Button();
            this.Urgent = new System.Windows.Forms.CheckBox();
            this.Important = new System.Windows.Forms.CheckBox();
            this.DescriptionLB = new System.Windows.Forms.Label();
            this.TitleLB = new System.Windows.Forms.Label();
            this.lB = new System.Windows.Forms.Label();
            this.lA = new System.Windows.Forms.Label();
            this.lC = new System.Windows.Forms.Label();
            this.lD = new System.Windows.Forms.Label();
            this.PanelB = new System.Windows.Forms.FlowLayoutPanel();
            this.PanelA = new System.Windows.Forms.FlowLayoutPanel();
            this.PanelC = new System.Windows.Forms.FlowLayoutPanel();
            this.PanelD = new System.Windows.Forms.FlowLayoutPanel();
            this.DetailPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DetailPanel
            // 
            this.DetailPanel.Controls.Add(this.SaveBT);
            this.DetailPanel.Controls.Add(this.DescriptionTB);
            this.DetailPanel.Controls.Add(this.TitleTB);
            this.DetailPanel.Controls.Add(this.Done);
            this.DetailPanel.Controls.Add(this.CancelBT);
            this.DetailPanel.Controls.Add(this.CreateNewTask);
            this.DetailPanel.Controls.Add(this.Urgent);
            this.DetailPanel.Controls.Add(this.Important);
            this.DetailPanel.Controls.Add(this.DescriptionLB);
            this.DetailPanel.Controls.Add(this.TitleLB);
            this.DetailPanel.Location = new System.Drawing.Point(617, 13);
            this.DetailPanel.Name = "DetailPanel";
            this.DetailPanel.Size = new System.Drawing.Size(235, 425);
            this.DetailPanel.TabIndex = 4;
            // 
            // SaveBT
            // 
            this.SaveBT.Location = new System.Drawing.Point(133, 295);
            this.SaveBT.Name = "SaveBT";
            this.SaveBT.Size = new System.Drawing.Size(75, 23);
            this.SaveBT.TabIndex = 9;
            this.SaveBT.Text = "Save";
            this.SaveBT.UseVisualStyleBackColor = true;
            this.SaveBT.Visible = false;
            this.SaveBT.Click += new System.EventHandler(this.SaveBT_Click);
            // 
            // DescriptionTB
            // 
            this.DescriptionTB.Location = new System.Drawing.Point(9, 97);
            this.DescriptionTB.Multiline = true;
            this.DescriptionTB.Name = "DescriptionTB";
            this.DescriptionTB.ReadOnly = true;
            this.DescriptionTB.Size = new System.Drawing.Size(134, 59);
            this.DescriptionTB.TabIndex = 8;
            // 
            // TitleTB
            // 
            this.TitleTB.Location = new System.Drawing.Point(6, 46);
            this.TitleTB.Name = "TitleTB";
            this.TitleTB.ReadOnly = true;
            this.TitleTB.Size = new System.Drawing.Size(137, 20);
            this.TitleTB.TabIndex = 7;
            // 
            // Done
            // 
            this.Done.AutoSize = true;
            this.Done.Location = new System.Drawing.Point(6, 244);
            this.Done.Name = "Done";
            this.Done.Size = new System.Drawing.Size(52, 17);
            this.Done.TabIndex = 6;
            this.Done.Text = "Done";
            this.Done.UseVisualStyleBackColor = true;
            this.Done.CheckedChanged += new System.EventHandler(this.Done_CheckedChanged);
            // 
            // CancelBT
            // 
            this.CancelBT.Location = new System.Drawing.Point(31, 295);
            this.CancelBT.Name = "CancelBT";
            this.CancelBT.Size = new System.Drawing.Size(75, 23);
            this.CancelBT.TabIndex = 5;
            this.CancelBT.Text = "Cancel";
            this.CancelBT.UseVisualStyleBackColor = true;
            this.CancelBT.Visible = false;
            this.CancelBT.Click += new System.EventHandler(this.CancelBT_Click);
            // 
            // CreateNewTask
            // 
            this.CreateNewTask.Location = new System.Drawing.Point(133, 295);
            this.CreateNewTask.Name = "CreateNewTask";
            this.CreateNewTask.Size = new System.Drawing.Size(75, 23);
            this.CreateNewTask.TabIndex = 4;
            this.CreateNewTask.Text = "AddTask";
            this.CreateNewTask.UseVisualStyleBackColor = true;
            this.CreateNewTask.Click += new System.EventHandler(this.CreateNewTask_Click);
            // 
            // Urgent
            // 
            this.Urgent.AutoSize = true;
            this.Urgent.Enabled = false;
            this.Urgent.Location = new System.Drawing.Point(85, 180);
            this.Urgent.Name = "Urgent";
            this.Urgent.Size = new System.Drawing.Size(58, 17);
            this.Urgent.TabIndex = 3;
            this.Urgent.Text = "Urgent";
            this.Urgent.UseVisualStyleBackColor = true;
            // 
            // Important
            // 
            this.Important.AutoSize = true;
            this.Important.Enabled = false;
            this.Important.Location = new System.Drawing.Point(9, 180);
            this.Important.Name = "Important";
            this.Important.Size = new System.Drawing.Size(70, 17);
            this.Important.TabIndex = 2;
            this.Important.Text = "Important";
            this.Important.UseVisualStyleBackColor = true;
            // 
            // DescriptionLB
            // 
            this.DescriptionLB.AutoSize = true;
            this.DescriptionLB.Location = new System.Drawing.Point(6, 80);
            this.DescriptionLB.Name = "DescriptionLB";
            this.DescriptionLB.Size = new System.Drawing.Size(60, 13);
            this.DescriptionLB.TabIndex = 1;
            this.DescriptionLB.Text = "Description";
            // 
            // TitleLB
            // 
            this.TitleLB.AutoSize = true;
            this.TitleLB.Location = new System.Drawing.Point(3, 20);
            this.TitleLB.Name = "TitleLB";
            this.TitleLB.Size = new System.Drawing.Size(30, 13);
            this.TitleLB.TabIndex = 0;
            this.TitleLB.Text = "Tilte:";
            // 
            // lB
            // 
            this.lB.AutoSize = true;
            this.lB.Location = new System.Drawing.Point(48, 239);
            this.lB.Name = "lB";
            this.lB.Size = new System.Drawing.Size(160, 13);
            this.lB.TabIndex = 0;
            this.lB.Text = "Click here to add a Type B Task";
            this.lB.Click += new System.EventHandler(this.label1_Click);
            // 
            // lA
            // 
            this.lA.AutoSize = true;
            this.lA.Location = new System.Drawing.Point(344, 239);
            this.lA.Name = "lA";
            this.lA.Size = new System.Drawing.Size(160, 13);
            this.lA.TabIndex = 5;
            this.lA.Text = "Click here to add a Type A Task";
            this.lA.Click += new System.EventHandler(this.lA_Click);
            // 
            // lC
            // 
            this.lC.AutoSize = true;
            this.lC.Location = new System.Drawing.Point(344, 472);
            this.lC.Name = "lC";
            this.lC.Size = new System.Drawing.Size(160, 13);
            this.lC.TabIndex = 6;
            this.lC.Text = "Click here to add a Type C Task";
            this.lC.Click += new System.EventHandler(this.lC_Click);
            // 
            // lD
            // 
            this.lD.AutoSize = true;
            this.lD.Location = new System.Drawing.Point(48, 472);
            this.lD.Name = "lD";
            this.lD.Size = new System.Drawing.Size(161, 13);
            this.lD.TabIndex = 7;
            this.lD.Text = "Click here to add a Type D Task";
            this.lD.Click += new System.EventHandler(this.lD_Click);
            // 
            // PanelB
            // 
            this.PanelB.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelB.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.PanelB.Location = new System.Drawing.Point(40, 33);
            this.PanelB.Name = "PanelB";
            this.PanelB.Size = new System.Drawing.Size(232, 198);
            this.PanelB.TabIndex = 8;
            // 
            // PanelA
            // 
            this.PanelA.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelA.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.PanelA.Location = new System.Drawing.Point(347, 33);
            this.PanelA.Name = "PanelA";
            this.PanelA.Size = new System.Drawing.Size(232, 198);
            this.PanelA.TabIndex = 9;
            // 
            // PanelC
            // 
            this.PanelC.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelC.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.PanelC.Location = new System.Drawing.Point(347, 266);
            this.PanelC.Name = "PanelC";
            this.PanelC.Size = new System.Drawing.Size(232, 198);
            this.PanelC.TabIndex = 9;
            // 
            // PanelD
            // 
            this.PanelD.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PanelD.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.PanelD.Location = new System.Drawing.Point(40, 266);
            this.PanelD.Name = "PanelD";
            this.PanelD.Size = new System.Drawing.Size(232, 198);
            this.PanelD.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 511);
            this.Controls.Add(this.PanelD);
            this.Controls.Add(this.PanelC);
            this.Controls.Add(this.PanelA);
            this.Controls.Add(this.PanelB);
            this.Controls.Add(this.lD);
            this.Controls.Add(this.lC);
            this.Controls.Add(this.lA);
            this.Controls.Add(this.lB);
            this.Controls.Add(this.DetailPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.DetailPanel.ResumeLayout(false);
            this.DetailPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel DetailPanel;
        private System.Windows.Forms.Label lB;
        private System.Windows.Forms.Label lA;
        private System.Windows.Forms.Label lC;
        private System.Windows.Forms.Label lD;
        private System.Windows.Forms.Label DescriptionLB;
        private System.Windows.Forms.Label TitleLB;
        private System.Windows.Forms.Button CancelBT;
        private System.Windows.Forms.Button CreateNewTask;
        private System.Windows.Forms.CheckBox Urgent;
        private System.Windows.Forms.CheckBox Important;
        private System.Windows.Forms.CheckBox Done;
        private System.Windows.Forms.TextBox TitleTB;
        private System.Windows.Forms.TextBox DescriptionTB;
        private System.Windows.Forms.FlowLayoutPanel PanelB;
        private System.Windows.Forms.FlowLayoutPanel PanelA;
        private System.Windows.Forms.FlowLayoutPanel PanelC;
        private System.Windows.Forms.FlowLayoutPanel PanelD;
        private System.Windows.Forms.Button SaveBT;
    }
}

