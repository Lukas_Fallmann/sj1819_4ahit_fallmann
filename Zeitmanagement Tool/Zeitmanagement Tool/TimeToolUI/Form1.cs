﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimetoolDLL;

namespace TimeToolUI
{
    public partial class Form1 : Form
    {
        public TimeManagement t;
        public Form1()
        {
            t = new TimeManagement("time.xml");
            InitializeComponent();
            LoadTasksToUI();
        }


        private void LoadTasksToUI()
        {
            foreach (Task task in t.GetTasks(State.A))
            {
                Label l = new Label { Text = task.Title };
                l.AutoSize = true;
                l.Click += ShowDetails;
                if (task.Done)
                    l.ForeColor = Color.Gray;
                l.Parent = PanelA;
            }
            foreach (Task task in t.GetTasks(State.B))
            {
                Label l = new Label { Text = task.Title };
                l.Click += ShowDetails;
                if (task.Done)
                    l.ForeColor = Color.Gray;
                l.Parent = PanelB;
            }
            foreach (Task task in t.GetTasks(State.C))
            {
                Label l = new Label { Text = task.Title };
                l.Click += ShowDetails;
                if (task.Done)
                    l.ForeColor = Color.Gray;
                l.Parent = PanelC;
            }
            foreach (Task task in t.GetTasks(State.D))
            {
                Label l = new Label { Text = task.Title };
                l.Click += ShowDetails;
                if (task.Done)
                    l.ForeColor = Color.Gray;
                l.Parent = PanelD;
            }
        }

        private void ShowDetails(object sender, EventArgs e)
        {
            Label l = (Label)sender;
            foreach (Task task in t.Tasks)
            {
                if (l.Text == task.Title)
                {
                    TitleTB.Text = task.Title;
                    DescriptionTB.Text = task.Description;
                    Important.Checked = task.Important;
                    Urgent.Checked = task.Urgent;
                    Done.Checked = task.Done;
                    break;
                }
            }
        }

        private void Done_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Task task in t.Tasks)
            {
                if (TitleTB.Text == task.Title)
                {
                    task.Done = Done.Checked;
                    break;
                }
            }
            ReLoad();
            
        }

        private void CreateNewTask_Click(object sender, EventArgs e)
        {
            Urgent.Enabled = true;
            Important.Enabled = true;
            Done.Enabled = false;
            CancelBT.Visible = true;
            CreateNewTask.Visible = false;
            TitleTB.ReadOnly = false;
            DescriptionTB.ReadOnly = false;
            SaveBT.Visible = true;
        }

        private void SaveBT_Click(object sender, EventArgs e)
        {
            if(TitleTB.Text!=string.Empty&&DescriptionTB.Text!=string.Empty)
            {
                t.AddTask(new Task(TitleTB.Text, DescriptionTB.Text, Important.Checked, Urgent.Checked));
                ReLoad();
            }
            else MessageBox.Show("Bitte alle Felder Ausfüllen");

        }

        private void CancelBT_Click(object sender, EventArgs e)
        {
            ReLoad();
            
        }
        private void ReLoad()
        {
            TitleTB.Text = string.Empty;
            DescriptionTB.Text = string.Empty;
            Urgent.Enabled = false;
            Important.Enabled = false;
            Done.Enabled = true;
            CancelBT.Visible = false;
            CreateNewTask.Visible = true;
            TitleTB.ReadOnly = true;
            DescriptionTB.ReadOnly = true;
            SaveBT.Visible = false;
            PanelA.Controls.Clear();
            PanelB.Controls.Clear();
            PanelC.Controls.Clear();
            PanelD.Controls.Clear();
            LoadTasksToUI();
        }

        private void lA_Click(object sender, EventArgs e)
        {
            CreateNewTask_Click(sender, e);
            Important.Checked = true;
            Urgent.Checked = true;
        }

        private void lD_Click(object sender, EventArgs e)
        {
            CreateNewTask_Click(sender, e);
            Important.Checked = false;
            Urgent.Checked = false;
        }

        private void lC_Click(object sender, EventArgs e)
        {
            CreateNewTask_Click(sender, e);
            Important.Checked = false;
            Urgent.Checked = true;
        }
        private void label1_Click(object sender, EventArgs e)
        {
            CreateNewTask_Click(sender, e);
            Important.Checked = true;
            Urgent.Checked = false;

        }
    }
}
