﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Archiver
{
    public partial class Form1 : Form
    {
        private FolderBrowserDialog fb = new FolderBrowserDialog();
        private OpenFileDialog of = new OpenFileDialog();

        public Form1()
        {
            InitializeComponent();
        }

        private void archiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            fb.SelectedPath = @"C:\Users\lukas\Documents\Schule\SEW\sj1819_4ahit_fallmann\UI-Threads\UI-Threads\bin\Debug";
            DialogResult dr = fb.ShowDialog(this);

            //string[] fileNames=Directory.GetFiles(fb.SelectedPath,"*.txt");

            //foreach(string s in fileNames)
            //{
            //    Append(s);
            //}

            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            //MessageBox.Show(fb.SelectedPath);
            // BackGroundWorker starten
            this.backgroundWorker1.RunWorkerAsync();



        }
        private void backgroundWorker1_ProgressChanged_1(object sender, ProgressChangedEventArgs e)
        {
            // ProgressBar einstellen
            progressBar1.Value = e.ProgressPercentage;

        }
        private void backgroundWorker1_RunWorkerCompleted_1(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show((string)e.Result);
            this.progressBar1.Value = 100;
        }
        //private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        //{

        //}


        private void dearchiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            DialogResult dr = of.ShowDialog(this);

            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            this.backgroundWorker2.RunWorkerAsync();

        }

        private void Append(string path)
        {
            StreamReader sr = new StreamReader(path);
            StreamWriter sw = new StreamWriter("Archive.arc",true);
            while(!sr.EndOfStream)
            {
                sw.WriteLine(sr.ReadLine());
            }
            sw.WriteLine("§");
            sw.WriteLine(path);
            sw.Flush();
            sw.Close();
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {

            string[] fileNames = Directory.GetFiles(fb.SelectedPath, "*.txt");
            int counter = 0;
            foreach (string s in fileNames)
            {
                Append(s);
                counter++;
                Thread.Sleep(1500);
                this.backgroundWorker1.ReportProgress(counter * 100 / fileNames.Length);
            }
            e.Result = counter + " File(s) archiviert";

        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            StreamReader sr = new StreamReader(of.FileName);
            string dirName = Path.GetFileName(of.FileName);
           
            string files ="";
            int counter = 0;
            string[] fileNames=new string[10];
            while (!sr.EndOfStream)
            {   
                string line= sr.ReadLine();
                files += line;
                if(line=="§")
                {
                    fileNames[counter] =Path.GetFileName( sr.ReadLine());
                    counter++;
                }


            }

            string[] fileArray = files.Split('§');
            for (int i = 0; i < counter; i++)
            {
                StreamWriter sw = new StreamWriter(@"C:\Users\lukas\Documents\Schule\SEW\sj1819_4ahit_fallmann\Archiver\Archiver\bin\DebugArchive" + @"\" +fileNames[i]);
                sw.Write(fileArray[i]);
                sw.Flush();
                sw.Close();

                this.backgroundWorker2.ReportProgress(i * 100 / counter);
            }
            e.Result = counter + " File(s) dearchiviert";
           

        }
        private void backgroundWorker2_ProgressChanged_1(object sender, ProgressChangedEventArgs e)
        {
            // ProgressBar einstellen
            progressBar1.Value = e.ProgressPercentage;

        }
        private void backgroundWorker2_RunWorkerCompleted_1(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show((string)e.Result);
            this.progressBar1.Value = 100;
        }



    }
}
