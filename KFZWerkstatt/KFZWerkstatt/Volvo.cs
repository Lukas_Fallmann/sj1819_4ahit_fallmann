﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZWerkstatt
{
    class Volvo:Auto
    {
        public Volvo()
        {
            hersteller = "Volvo";
        }
        public override string ToString()
        {
            return hersteller + " - " + Model; 
        }
    }
}
