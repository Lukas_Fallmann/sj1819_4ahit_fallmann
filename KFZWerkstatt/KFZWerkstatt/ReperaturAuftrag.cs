﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZWerkstatt
{
    class ReperaturAuftrag<H, A> where H : Auto, new() where A : IArtikel
    {
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public int Reperaturnummer { get; set; }
        public H Hersteller { get; set; }
        public List<A> Bestandteile { get; set; }

        public ReperaturAuftrag(string vorName, string nachName, string Modelbezeichnung)
        {
            this.Vorname = vorName;
            this.Nachname = nachName;
            Hersteller = new H();
            Hersteller.Model = Modelbezeichnung;
            Random r = new Random();
            Reperaturnummer = r.Next(1, 200);
            Bestandteile = new List<A>();

        }

        public void ArtikelAdd(A artikel)
        {
            Bestandteile.Add(artikel);
            artikel.NachBestellen();
        }
        public override string ToString()
        {
            string baselinfo = "Reperaturauftrag " + Reperaturnummer + " für Kunde: " + Vorname + " " + Nachname+"\n";

            string carinfo = "\nAutomarke: " + Hersteller.ToString()+"\n";

            string sartikel = "\nBestandteile: \n ----------------------\n";
            int gesPreis=0;
            foreach (A artikel in Bestandteile)
            {
                sartikel += artikel.ToString() + "\n";
                gesPreis += artikel.GesPreis();
            }
            sartikel+="----------------------\n";
            return baselinfo + carinfo + sartikel + "Summe: " + gesPreis;
            
        }
    }
}
