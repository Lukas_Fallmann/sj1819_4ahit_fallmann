﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZWerkstatt
{
    class Program
    {
        static void Main(string[] args)
        {
            ReperaturAuftrag<Volvo, MotorTeile> rA = new ReperaturAuftrag<Volvo, MotorTeile>("Hans", "Mayer", "V40");
            rA.ArtikelAdd(new MotorTeile(2, 50, "Kolben"));
            rA.ArtikelAdd(new MotorTeile(1, 200, "Turbocharger"));

            Console.WriteLine(rA.ToString());
        }
    }
}
