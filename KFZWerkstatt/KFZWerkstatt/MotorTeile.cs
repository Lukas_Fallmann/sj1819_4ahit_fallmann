﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KFZWerkstatt
{
    class MotorTeile:IArtikel
    {
        public int Menge { get; set; }
        public int Preis { get; set; }
        public int GesamtP { get; set; }
        public string Bezeichnung { get; set; }

        public MotorTeile(int menge,int preis,string bezeichnung)
        {
            this.Menge = menge;
            this.Preis = preis;
            this.Bezeichnung = bezeichnung;
            this.GesamtP = menge * preis;
        }

        public override string ToString()
        {
            return Menge + " Stk " + Bezeichnung + " - " + GesamtP + " Euro";
        }

        public int GesPreis()
        {
            return GesamtP;
        }

        public void NachBestellen()
        {
            
        }
    }
}
