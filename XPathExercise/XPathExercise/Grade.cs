﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPathExercise
{
    class Grade
    {
        public double Points { get; set; }
        public double GradeNr { get; set; }
        public Grade(double points,double grade)
        {
            this.Points = points;
            this.GradeNr = grade;
        }
    }
}
