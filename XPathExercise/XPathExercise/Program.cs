﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XPathExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument results = new XmlDocument();
            results.Load("Result.tea");
            XmlDocument test = new XmlDocument();
            test.Load("Test_777.tea");

            //Part A of the execise
            //Students with more than 30 Points
            XmlNodeList pupils = results.SelectNodes("/TestResult/Pupil[Punkte>30]");
            Console.WriteLine(pupils.Count);

            Console.WriteLine();

            //Name of Pupil with the ID 941
            XmlNode p941 = results.SelectSingleNode("/TestResult/Pupil[@ID=941]/Nachname");
            Console.WriteLine(p941.InnerText);

            Console.WriteLine();

            //Points summed up and compared
            double pointcounter = 0;
            XmlNodeList points = results.SelectNodes("/TestResult/Pupil[Punkte>=0]/Punkte");
            XmlNode pointssum = results.SelectSingleNode("/TestResult/@SumPunkte");
            foreach (XmlNode node in points)
            {
                string s = node.InnerText.Replace('.', ',');
                pointcounter += Convert.ToDouble(s);
            }
            Console.WriteLine(Convert.ToDouble(pointssum.InnerText.Replace('.', ',')) == pointcounter);

            Console.WriteLine();

            //Average Points
            pointcounter = 0;
            foreach (XmlNode node in points)
            {
                string s = node.InnerText.Replace('.', ',');
                pointcounter += Convert.ToDouble(s);
            }
            Console.WriteLine(pointcounter / points.Count);

            Console.WriteLine();

            //Show XML
            XmlNodeList pupilsabsolute = results.SelectNodes("/TestResult/Pupil");
            foreach (XmlNode pupil in pupilsabsolute)
            {
                Console.WriteLine(pupil.InnerText);
            }

            Console.WriteLine();

            //Part B of the execise
            //Wieviele Aufgaben haben mehr als 2 Punkte
            XmlNodeList morethantwo = test.SelectNodes("/Test/EntryList/Exercise[Points>2]");
            Console.WriteLine(morethantwo.Count);

            Console.WriteLine();

            //How many Answers with the ID 16
            XmlNodeList answersfor16 = test.SelectNodes("/Test/EntryList/Exercise[@ID=16]/subExerciseNode/ExMC/Value");
            Console.WriteLine(answersfor16.Count);

            Console.WriteLine();

            //Double Checking Grades
            List<Grade> notenmitgrenzen = new List<Grade>();
            XmlNodeList grades = test.SelectNodes("/Test/Infos/Grading/Schulnoten/Grenze");
            foreach (XmlNode grade in grades)
            {
                string s = grade.Attributes["Note"].InnerText.Replace('.', ',');
                notenmitgrenzen.Add(new Grade(Convert.ToInt32(grade.Attributes["Grenze"].InnerText), Convert.ToDouble(s)));
            }

            pupils = results.SelectNodes("/TestResult/Pupil[Punkte>=0]");
            foreach (XmlNode pupil in pupils)
            {
                bool legit = false;

                double note = Convert.ToDouble(pupil["Note"].InnerText.Replace('.', ','));
                double punkte = Convert.ToDouble(pupil["Punkte"].InnerText.Replace('.', ','));
                for (int i = 0; i < notenmitgrenzen.Count; i++)
                {
                    if (notenmitgrenzen[i].GradeNr == note && punkte >= notenmitgrenzen[i].Points)
                        legit = true;
                }
                if (!legit)
                    Console.WriteLine("Fehler bei " + pupil["Nachname"].InnerText + " " + pupil["Vorname"].InnerText);
            }
        }
    }
}
