﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Number2Text_Conversion
{
    class NumberoutofRangeException:Exception
    {
        public NumberoutofRangeException()
        {

        }
        public NumberoutofRangeException(string message):base (message)
        {

        }
        public NumberoutofRangeException(string message,Exception inner):base(message,inner)
        {

        }
    }
}
