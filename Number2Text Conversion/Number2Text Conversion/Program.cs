﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Number2Text_Conversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Utility u = new Utility();
            Console.WriteLine(u.Conv2Text(1001));
            Console.WriteLine();
            Console.WriteLine(u.Conv2Text(999999999999));
            Console.WriteLine();
            Console.WriteLine(u.Conv2Text(1000000));
            Console.WriteLine();
            Console.WriteLine(u.Conv2Text(54321));
            Console.WriteLine();
            Console.WriteLine(u.Conv2Text(1543234));
            u.Tell(1001);
        
        }
    }
}
