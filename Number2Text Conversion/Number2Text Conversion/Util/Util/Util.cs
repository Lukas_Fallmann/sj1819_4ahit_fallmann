﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;

namespace Number2Text_Conversion
{
    public class Utility
    {
        protected Dictionary<long, string> Numbers = new Dictionary<long, string>
            {
                { 1, "eins" },
                { 2, "zwei" },
                { 3, "drei" },
                { 4, "vier" },
                { 5, "fünf" },
                { 6, "sechs" },
                { 7, "sieben" },
                { 8, "acht" },
                { 9, "neun" },
                { 10, "zehn" },
                { 11, "elf" },
                { 12, "zwölf" },
                { 17, "siebzehn" },
                { 20, "zwanzig" },
                { 30, "dreißig" },
                { 40, "vierzig" },
                { 50, "fünfzig" },
                { 60, "sechzig" },
                { 70, "siebzig" },
                { 80, "achtzig" },
                { 90, "neunzig" },
                { 100, "einhundert" },
                { 1000, "eintausend" },
                { 1000000, "einemilion" },
            };
        public string Conv2Text(long number)
        {


            if (Numbers.ContainsKey(number))
            {
                return Numbers[number];
            }

            if (0 <= number && number < 1000)
            {
                if (number == 0)
                    return "null";
                //int einerStelle= (int)number % 10;
                //string einerSt=string.Empty;
                //    if (einerStelle == 1)
                //        einerSt = "ein";
                //    else
                //        einerSt = Numbers[einerStelle];
                //int zehnerStelle = (int)number - einerStelle;
                //string zehnerSt = Numbers[zehnerStelle];
                //if (zehnerStelle == 10)
                //    return einerSt + "zehn";
                //else
                //    return einerSt + "und" + zehnerSt;

                return GetTriplet(Convert.ToString(number));
            }

            if (1000 <= number && number < 1000000000000)
            {
                string numberstr = Convert.ToString(number);
                int i = numberstr.Length;
                switch (i % 3)
                {
                    case (1):
                        numberstr = "00" + numberstr;
                        break;
                    case (2):
                        numberstr = "0" + numberstr;
                        break;
                }
                int runtime = 0;
                string finalstring = string.Empty;
                for (int j = numberstr.Length; j >= 3; j -= 3)
                {
                    string helpstring;
                    helpstring = GetTriplet(numberstr.Substring(j - 3, 3));
                    switch (runtime)
                    {
                        case 0:
                            if (helpstring == "ein")
                                finalstring = helpstring + "s";
                            else
                                finalstring = helpstring;
                            break;
                        case 1:
                            finalstring = helpstring + "tausend" + finalstring;
                            break;
                        case 2:
                            finalstring = helpstring + "milionen" + finalstring;
                            break;
                        case 3:
                            finalstring = helpstring + "miliarden" + finalstring;
                            break;
                    }
                    runtime++;
                }
                return finalstring;
            }

            return string.Empty;



        }
        private string GetTriplet(string triple)
        {
            //initializing variables
            string zehnerundeiner = string.Empty;
            string hunderterSt = string.Empty;
            string zehnerSt = string.Empty;
            string einerSt = string.Empty;
            int number = Convert.ToInt32(triple);
            //getting the value of the first number of the string e.g.1
            int einerStelle = (int)number % 10;
            //asigning the number to a helpstring but only if its not empty
            if (einerStelle != 0)
            {
                if (einerStelle == 1)
                    einerSt = "ein";
                else
                    einerSt = Numbers[einerStelle];
            }
            //getting the value of the second number of the string e.g. 10
            int zehnerStelle = (number - einerStelle) % 100;
            //asigning the number to a helpstring if it is not 0
            if (zehnerStelle != 0)
                zehnerSt = Numbers[zehnerStelle];
            //merging "ZehnerStelle" and "Einerstelle" with special cases
            if (einerStelle == 0 || zehnerStelle == 0)
            {//case one of them is zero
                if (zehnerStelle == 0)
                    zehnerundeiner = einerSt;
                if (einerStelle == 0)
                    zehnerundeiner = zehnerSt;

            }
            else
            {//case "zehnerStelle" = 10 add +"zehn" to the "Einer"
                if (zehnerStelle == 10)
                    zehnerundeiner = einerSt + "zehn";
                zehnerundeiner = einerSt + "und" + zehnerSt;
            }
            //getting the value of the third number of the String e.g. 100  
            int hunderterStelle = number - (zehnerStelle + einerStelle);
            //merging all three numbers if "Hunderter" is not 0 
            if (hunderterStelle != 0)
            {
                if (Numbers.ContainsKey(hunderterStelle))
                    hunderterSt = Numbers[hunderterStelle];
                else
                    hunderterSt = Numbers[hunderterStelle / 100] + "hundert";
                return hunderterSt + zehnerundeiner;
            }
            // if there is no "Hunderter" return only "zehnerundeiner"
            else
                return zehnerundeiner;
        }

        public void Tell(long number)
        {
            SpeechSynthesizer cortana = new SpeechSynthesizer();
            cortana.SetOutputToDefaultAudioDevice();
            cortana.Speak(Conv2Text(number));
        }
    }
}
