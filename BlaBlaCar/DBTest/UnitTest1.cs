﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlaBlaCar;
using System.Linq;

namespace DBTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Insert()
        {
            CarSharing cs = new CarSharing();
            Car car = new Car { Color = "blue", Brand = "Audi", Model = "A4" };
            cs.Cars.Add(car);
            cs.SaveChanges();
            //var result = from c in cs.Cars
            //             where c.Brand == "Audi" && c.Color == "blue" && c.Model == "A4"
            //             select c;
            //var car2 = result.FirstOrDefault<Car>();
            Assert.IsNotNull(cs.Cars.Count());            
        }

        [TestMethod]

        public void Select()
        {
            CarSharing cs = new CarSharing();
            var result = from c in cs.Cars
                         where c.Brand == "Audi" && c.Color == "blue" && c.Model == "A4"
                         select c;
            Assert.IsNotNull(result);
        }

        [TestMethod]

        public void Delete()
        {
            CarSharing cs = new CarSharing();
            int beginLength=cs.Cars.Count();
            Car car = new Car { Color = "blue", Brand = "Audi", Model = "A4" };
            cs.Cars.Add(car);
            cs.SaveChanges();
            cs.Cars.Remove(car);
            cs.SaveChanges();
            Assert.AreEqual(beginLength, cs.Cars.Count());

        }

        [TestMethod]

        public void Update()
        {
            CarSharing cs = new CarSharing();
            Car car = cs.Cars.First<Car>();
            car.Color = "Red";
            cs.SaveChanges();
            var result = from c in cs.Cars
                         where c.Brand == "Audi" && c.Color == "Red" && c.Model == "A4"
                         select c;
            Assert.IsNotNull(result);
        }

    }
}
