﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BlaBlaCar
{
    public enum Gender
    {
        Male,Female
    }
    [Table("Users")]
    public class User
    {
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public Gender Gender { get; set; }

        public void CancelRide(int RouteID)
        {
            throw new System.NotImplementedException();
        }
    }
}
