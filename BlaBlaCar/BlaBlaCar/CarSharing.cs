﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BlaBlaCar
{
    public class CarSharing:DbContext
    {
        public CarSharing():base("SharedRides")
        {
            Database.SetInitializer<CarSharing>(new CreateDatabaseIfNotExists<CarSharing>());

        }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Route> Routes { get; set; }
    }
}
