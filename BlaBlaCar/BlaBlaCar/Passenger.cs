﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlaBlaCar
{
    [Table("Passengers")]
    public class Passenger:User
    {
        public int PassengerID { get; set; }

        public Route CurrentRoute
        {
            get;
            set;
            
        }

        public void JoinRoute(int RouteID, bool isLadyCar)
        {
            throw new System.NotImplementedException();
        }

        public void rateDriver(int driverID, Rating rating)
        {
            throw new System.NotImplementedException();
        }

        public void Pay(PreferedPayment payMethod)
        {
            throw new System.NotImplementedException();
        }

        public void changeRide(DateTime startTime, string endPostal, string startPostal, bool isLadyCar)
        {
            throw new System.NotImplementedException();
        }

        public void Update()
        {
            throw new System.NotImplementedException();
        }
    }
}
