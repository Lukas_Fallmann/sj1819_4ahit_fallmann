﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlaBlaCar
{
    public interface IPayment
    {
        void Pay();
    }
}