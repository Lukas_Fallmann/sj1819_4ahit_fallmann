﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlaBlaCar
{
    [Table("Drivers")]
    public class Driver:User
    {
        public int DriverId { get; set; }
        public float Rating { get; set; }
        public string PaymentInfo { get; set; }
        public PreferedPayment PreferedPayment { get; set; }
        public Car Car { get; set; }

        public void CreateNewRoute(string endPostal, string startPostal, bool isLadyCar, float price,Driver driver, DateTime startTime)
        {
            throw new System.NotImplementedException();
        }

        public void ChoosePayment(PreferedPayment prefPayment)
        {
            throw new System.NotImplementedException();
        }

        public void ChangeRide(int RideID, string endPostal, bool isLadyCar, float price, string startPostal, DateTime startTime)
        {
            throw new System.NotImplementedException();
        }
    }
    public enum PreferedPayment
    {
        Paypal,Bar,SofortÜberweisung
    }
}
