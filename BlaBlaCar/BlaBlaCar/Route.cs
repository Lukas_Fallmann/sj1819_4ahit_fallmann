﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlaBlaCar
{
    
    [Table("Routes")]
    public class Route
    {
        public int RouteId { get; set; }
        public string StartPostal { get; set; }
        public string EndPostal { get; set; }
        public bool IsLadyCar { get; set; }
        public Driver Driver { get; set; }
        public float Price { get; set; }
        public List<Passenger> Passengers { get; set; }

        public int StartTime
        {
            get => default(int);
            set
            {
            }
        }

        public void Done()
        {
            throw new System.NotImplementedException();
        }

        public void Notify()
        {
            throw new System.NotImplementedException();
        }
    }
}
