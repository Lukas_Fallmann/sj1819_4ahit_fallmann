﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace ScreenSaver
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Random rnd = new Random();
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        int lastX, lastY,linecounter;
        public MainWindow()
        {
            InitializeComponent();
            lastX = rnd.Next(Convert.ToInt32(Zeichenflaeche.Width));
            lastY = rnd.Next(Convert.ToInt32(Zeichenflaeche.Height));
            linecounter = 0;
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();

        }
       

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Color rndcolor = Color.FromArgb((byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            Line l = new Line();
            SolidColorBrush brush = new SolidColorBrush(rndcolor);
            l.Stroke = brush;
            l.X1 = lastX;
            l.Y1 = lastY;
            int helpx = rnd.Next(Convert.ToInt32(Zeichenflaeche.Width));
            l.X2 = helpx;
            int helpy = rnd.Next(Convert.ToInt32(Zeichenflaeche.Height));
            l.Y2 = helpy;
            lastX = helpx;
            lastY = helpy;
            l.StrokeThickness = 3;
            Zeichenflaeche.Children.Add(l);
            linecounter+=2;
            if(linecounter>10)
            {
                linecounter-=2;
                Zeichenflaeche.Children.RemoveRange(Zeichenflaeche.Children.Count - 11, 2);
            }


            //Elipsis

            Ellipse cicle = new Ellipse {Width=rnd.Next(300),Height=rnd.Next(300) };
            cicle.StrokeThickness = 4;
            cicle.Stroke = brush;
            cicle.Margin = new Thickness(l.X1, l.Y1, 0, 0);
            Zeichenflaeche.Children.Add(cicle);


        }
    }
}
