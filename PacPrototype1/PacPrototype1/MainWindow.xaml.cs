﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
namespace PacPrototype1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Image myimage = new Image();

        public MainWindow()
        {
            InitializeComponent();
            myimage.RenderTransformOrigin = new Point(0.5, 0.5);
            KeyDown += new KeyEventHandler(steering);
            myimage.Loaded += Myimage_Loaded;
            Gamearea.Children.Add(myimage);

        }

        private void steering(object sender, KeyEventArgs e)
        {
           
            TransformGroup t = new TransformGroup();
            switch (e.Key)
            {
                case Key.Up:
                    RotateTransform up = new RotateTransform(-90);
                    TranslateTransform moveup = new TranslateTransform(0,-50);
                    t.Children.Add(up);
                    t.Children.Add(moveup);
                    myimage.RenderTransform = t;
                    break;
                case Key.Down:
                    RotateTransform down = new RotateTransform(90);
                    TranslateTransform movedown = new TranslateTransform(0, 50);
                    t.Children.Add(down);
                    t.Children.Add(movedown);
                    myimage.RenderTransform = t;
                    break;
                case Key.Left:
                    RotateTransform left = new RotateTransform(180);
                    TranslateTransform moveleft = new TranslateTransform(-50, 0);
                    t.Children.Add(left);
                    t.Children.Add(moveleft);
                    myimage.RenderTransform = t;
                    break;
                case Key.Right:
                    RotateTransform right = new RotateTransform(0);
                    TranslateTransform moveright = new TranslateTransform(50, 0);
                    t.Children.Add(right);
                    t.Children.Add(moveright);
                    myimage.RenderTransform = t;
                    break;
                case Key.Space:
                    myimage.RenderTransform = t;
                    break;

            }
            
        }

        private void Myimage_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage theImage = new BitmapImage();
            theImage.BeginInit();
            theImage.UriSource = (new Uri(@"C:\Users\lukas\Documents\Schule\SEW\sj1819_4ahit_fallmann\PacPrototype1\PacPrototype1\bin\Debug\pacman.gif"));
            theImage.EndInit();
            
            var image = sender as Image;
            
            ImageBehavior.SetAnimatedSource(image, theImage);
            image.Width = 50;
            image.Height = 50;


            Canvas.SetLeft(image, Gamearea.ActualWidth / 2);
            Canvas.SetTop(image, Gamearea.ActualHeight / 2);
        }
    }
}
