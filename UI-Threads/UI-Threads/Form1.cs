﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace UI_Threads
{
    delegate void StringArgReturningVoidDelegate(string text);
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        public Form1()
        {
            InitializeComponent();
            ResultTB.ReadOnly = true;
            timer1.Start();
            timer1.Tick += new EventHandler(timer1_Tick);

        }

    

        private void timer1_Tick(object sender, EventArgs e)
        {
            Thread.Sleep(100);
            ChangeColor();     
          
        }

        private void ChangeColor()
        {
            Color rndcolor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
            panel1.BackColor = rndcolor;
        }

        private void resultBT_Click(object sender, EventArgs e)
        {
            
            int number1=Convert.ToInt32(Number1.Text),number2= Convert.ToInt32(Number2.Text);
            Wrapper w = new Wrapper(number1, number2);
            
            //Thread calcGGT = new Thread(()=>berechneGgt(number1,number2))
            Thread calcGGT = new Thread(berechneGgt)
            {
                Name = "BackgroundFred",
                IsBackground = true,
            };
            calcGGT.Start(w);
            if(!calcGGT.IsAlive)
            timer1.Stop();
           

            this.Number1.Text = string.Empty;
            this.Number2.Text = string.Empty;

        }

        private void WritetoTXT(int result)
        {
            
            StreamWriter sw = new StreamWriter("results.txt",true);
            sw.WriteLine(result);
            sw.Flush();
            sw.Close();
        }
        //private void berechneGgt(int _zahl1, int _zahl2)
        private void berechneGgt(object w)
        {
            Wrapper nummern = (Wrapper)w;
            int zahl1;
            int zahl2;
            //zahl1 = _zahl1;
            //zahl2 = _zahl2;

            zahl1 = nummern.Zahl1;
            zahl2 = nummern.Zahl2;

            //Diese Variable wird bei Wertzuweisungen zwischen den Zahlen benutzt
            int temp = 0;
            //Der Rückgabewert zweier gegebener Zahlen.
            int ggt = 0;//Solange der Modulo der zwei zahlen nicht 0 ist,
                        //werden Zuweisungen entsprechend demEuklidischen Algorithmus ausgeführt.
            while (zahl1 % zahl2 != 0)
            {
                Thread.Sleep(4000);
                temp = zahl1 % zahl2;
                zahl1 = zahl2;
                zahl2 = temp;
            }

            ggt = zahl2;

            SetResult(ggt.ToString());
            WritetoTXT(ggt);
            

        }
        private void SetResult(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.ResultTB.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(SetResult);
                this.Invoke(d, text );
            }
            else
            {
                this.ResultTB.Text = text;
            }
        }
    }
}
