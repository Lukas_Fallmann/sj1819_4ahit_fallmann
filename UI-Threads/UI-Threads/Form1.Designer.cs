﻿namespace UI_Threads
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.Number1 = new System.Windows.Forms.TextBox();
            this.Number2 = new System.Windows.Forms.TextBox();
            this.resultBT = new System.Windows.Forms.Button();
            this.ResultTB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(383, 159);
            this.panel1.TabIndex = 0;
            // 
            // Number1
            // 
            this.Number1.Location = new System.Drawing.Point(25, 201);
            this.Number1.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Number1.Name = "Number1";
            this.Number1.Size = new System.Drawing.Size(70, 20);
            this.Number1.TabIndex = 1;
            // 
            // Number2
            // 
            this.Number2.Location = new System.Drawing.Point(107, 201);
            this.Number2.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Number2.Name = "Number2";
            this.Number2.Size = new System.Drawing.Size(73, 20);
            this.Number2.TabIndex = 2;
            // 
            // resultBT
            // 
            this.resultBT.Location = new System.Drawing.Point(188, 201);
            this.resultBT.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.resultBT.Name = "resultBT";
            this.resultBT.Size = new System.Drawing.Size(78, 20);
            this.resultBT.TabIndex = 3;
            this.resultBT.Text = "Calculate";
            this.resultBT.UseVisualStyleBackColor = true;
            this.resultBT.Click += new System.EventHandler(this.resultBT_Click);
            // 
            // ResultTB
            // 
            this.ResultTB.Location = new System.Drawing.Point(277, 201);
            this.ResultTB.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.ResultTB.Name = "ResultTB";
            this.ResultTB.Size = new System.Drawing.Size(90, 20);
            this.ResultTB.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 246);
            this.Controls.Add(this.ResultTB);
            this.Controls.Add(this.resultBT);
            this.Controls.Add(this.Number2);
            this.Controls.Add(this.Number1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox Number1;
        private System.Windows.Forms.TextBox Number2;
        private System.Windows.Forms.Button resultBT;
        private System.Windows.Forms.TextBox ResultTB;
    }
}

