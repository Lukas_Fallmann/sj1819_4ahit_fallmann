﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_Threads
{
    class Wrapper
    {
        public int Zahl1 { get; set; }
        public int Zahl2 { get; set; }
        public Wrapper(int zahl1,int zahl2)
        {
            this.Zahl1 = zahl1;
            this.Zahl2 = zahl2;
        }
    }
}
