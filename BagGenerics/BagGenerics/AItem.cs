﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagGenerics
{
    abstract class AItem
    {
        public int Size { get; set; }
        public string Description { get; set; }

        public int Weight { get; set; }

        public AItem(int size,string description)
        {
            this.Size = size;
            this.Description = description;
            this.Weight = this.GetWeight();
        }
        public int GetWeight()
        {
            int value=0;
            string txt = Description;
            for (int i = 0; i < txt.Length; i++)
                value+=(int)txt[i];
            return value * Size;
        }
    }
}
