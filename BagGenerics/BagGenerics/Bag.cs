﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagGenerics
{
    class Bag<T> where T:AItem
    {
        private int size;
        private List<T> elements = new List<T>();
       
        public Bag(int size)
        {
            this.size = size;
        }
        public void PutItem(T item)
        {
            if (elements.Count <= size)
                elements.Add(item);
            else
                Console.WriteLine("Tasche ist voll");
        }
        public T GetItem()
        {
            if (elements.Count > 0)
            {
                elements = elements.OrderBy(a => a.Weight).ThenBy(b => b.Size).ToList();

                T temp = elements[elements.Count-1];
                elements.RemoveAt(elements.Count-1);
             
                return temp;
            }
            else
                return default(T);
        }
        public string Content()
        {
            string content = string.Empty;
            foreach (T element in elements)
            {
                content+=element.ToString()+"\n";
            }
            return content;
        }
    }
}
