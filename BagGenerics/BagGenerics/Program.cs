﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagGenerics
{
    class Program
    {
        static void Main(string[] args)
        {

            //Bag<int> zahlenTasche = new Bag<int>(5);
            //zahlenTasche.PutItem(2);
            //zahlenTasche.PutItem(3);
            //zahlenTasche.PutItem(4);
            //zahlenTasche.PutItem(5);
            //zahlenTasche.PutItem(6);
            //int removedItem = zahlenTasche.GetItem();
            //int secondremovedItem = zahlenTasche.GetItem();
            //Console.WriteLine(zahlenTasche.Content());
            //Console.WriteLine(removedItem);
            //Console.WriteLine(secondremovedItem);

            Bag<Item> tasche = new Bag<Item>(5);

            tasche.PutItem(new Item(0, 2, "4"));
            tasche.PutItem(new Item(1, 1, "h"));

            Console.WriteLine(tasche.Content());
            Console.WriteLine(tasche.GetItem().ToString()+"\n");

            Console.WriteLine(tasche.Content());
        }
    }
}
