﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BagGenerics
{
    class Item:AItem
    {
        public int ID { get; set; }
        public Item(int id,int size,string description):base(size,description)
        {
            this.ID = id;
        }
        public override string ToString()
        {
            return "ID: "+ID + "\t Size: "+Size+"\t Description: "+Description;
        }
    }
}
