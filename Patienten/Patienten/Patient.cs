﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patienten
{
    class Patient
    {
        public int ID { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
        public string SV { get; set; }
        public List<Behandlung> Behandlungen { get; set; }
        public Patient(int id,string first,string last,string sv)
        {
            this.ID = id;
            this.First = first;
            this.Last = last;
            this.SV = sv;
            this.Behandlungen = new List<Behandlung>();
        }
    }
}
