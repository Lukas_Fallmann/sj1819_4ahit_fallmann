﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patienten
{
    class Behandlung
    {
        public int PatientenID { get; set; }
        public int BehandlungsNr { get; set; }
        public string BehandlungsTyp { get; set; }
        public string Dauer { get; set; }
        public DateTime Datum { get; set; }
        public Behandlung(int patientenID,int behandlungsNr, string behandlungstyp,string dauer,DateTime datum)
        {
            this.PatientenID = patientenID;
            this.BehandlungsNr = behandlungsNr;
            this.BehandlungsTyp = behandlungstyp;
            this.Dauer = dauer;
            this.Datum = datum;
        }
    }
}
