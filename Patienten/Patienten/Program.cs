﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Data.OleDb;

namespace Patienten
{
    class Program
    {

        static void Main(string[] args)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Patients></Patients>");

            XmlDeclaration xmldecl;

            xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            xmldecl.Encoding = "UTF-8";
            xmldecl.Standalone = "yes";

            string path = "patients.xml";
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmldecl, root);
            doc.Save(path);
            List<Patient> patienten = GetDataFromDB();

            WriteDatatoXML(patienten);

            if(CompareXMLtoDB(patienten,path))
                Console.WriteLine("plausibility check succesfull");
            else
                Console.WriteLine("plausibility check failed");
        }

        private static bool CompareXMLtoDB(List<Patient> patienten, string path)
        {
            List<Patient> patiententmp = new List<Patient>();
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNode node = doc.DocumentElement.SelectSingleNode("/Patients");

            foreach (XmlNode patient in node.ChildNodes)
            {
                Patient p = new Patient(Convert.ToInt32(patient.Attributes["ID"].InnerText), patient.Attributes["FirstName"].InnerText, patient.Attributes["LastName"].InnerText, patient.Attributes["SVNumber"].InnerText);
                foreach (XmlNode behandlungen in patient.ChildNodes)
                {
                    foreach (XmlNode behandlung  in behandlungen.ChildNodes)
                    {
                        string date=behandlung.Attributes["Date"].InnerText;
                        string dauer="", behandlungstyp="";
                        foreach (XmlNode behandlungsitem  in behandlung)
                        {
                            if (behandlungsitem.Name == "Dauer")
                                dauer = behandlungsitem.InnerText;
                            else
                                behandlungstyp = behandlungsitem.InnerText;
                        }
                        Behandlung b = new Behandlung(0, 0, behandlungstyp, dauer, Convert.ToDateTime(date));
                        p.Behandlungen.Add(b);
                    }
                }
                patiententmp.Add(p);
            }
            patiententmp.Sort((p1, p2) => p1.ID.CompareTo(p2.ID));
            patienten.Sort((p1, p2) => p1.ID.CompareTo(p2.ID));
            int count = 0;
            foreach (Patient p in patiententmp)
            {
                if (p.ID != patienten[count].ID || p.Last != patienten[count].Last || p.First != patienten[count].First || p.SV != patienten[count].SV)
                    return false;
                count++;
            }

            return true;
        }

        static List<Patient> GetDataFromDB()
        {
            List<Patient> patients = new List<Patient>();
            List<Behandlung> behandlungen = new List<Behandlung>();
            string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\lukas\Documents\Schule\SEW\sj1819_4ahit_fallmann\XML Start\Transfer1.mdb";
            OleDbConnection con = new OleDbConnection(connectionString);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "Select * from Patient";
            cmd.Connection = con;

            OleDbDataReader r = cmd.ExecuteReader();
            while (r.Read())
            {
                Patient p = new Patient(r.GetInt32(0), r.GetString(1), r.GetString(2), r.GetString(3));
                patients.Add(p);
                //XmlDocument doc = new XmlDocument();
                //doc.Load("patients.xml");

                //without attributes
                //XmlElement p = doc.CreateElement("Patient");
                //XmlElement d = doc.CreateElement("ID");
                //d.InnerText = Convert.ToString(id);
                //XmlElement last = doc.CreateElement("LastName");
                //last.InnerText = l;
                //XmlElement first = doc.CreateElement("FirstName");
                //first.InnerText = f;
                //XmlElement sV = doc.CreateElement("SVNumber");
                //sV.InnerText = sv;
                //XmlNode patients = doc.SelectSingleNode("Patients");
                //p.AppendChild(d);
                //p.AppendChild(last);
                //p.AppendChild(first);
                //p.AppendChild(sV);
                //patients.AppendChild(p);

                //with attributes

                //XmlElement p = doc.CreateElement("Patient");
                //XmlAttribute d = doc.CreateAttribute("ID");
                //d.Value = Convert.ToString(id);
                //p.Attributes.Append(d);
                //XmlAttribute last = doc.CreateAttribute("LastName");
                //last.Value = l;
                //p.Attributes.Append(last);
                //XmlAttribute first = doc.CreateAttribute("FirstName");
                //first.InnerText = f;
                //p.Attributes.Append(first);
                //XmlAttribute sV = doc.CreateAttribute("SVNumber");
                //sV.InnerText = sv;
                //p.Attributes.Append(sV);
                //XmlNode patients = doc.SelectSingleNode("Patients");
                //patients.AppendChild(p);

                //doc.Save("patients.xml");


            }
            r.Close();
            OleDbCommand cmd2 = new OleDbCommand();
            cmd2.CommandText = "Select p.PatientID,p.BehandlungsID,b.Bezeichnung,p.Dauer,p.Datum from (PatientBehandlungen p inner join Behandlung b on p.BehandlungsID=b.ID)";
            cmd2.Connection = con;
            OleDbDataReader r2 = cmd2.ExecuteReader();
            while (r2.Read())
            {
                Behandlung b = new Behandlung(r2.GetInt32(0), r2.GetInt32(1), r2.GetString(2), Convert.ToString(r2.GetInt32(3)), r2.GetDateTime(4));
                behandlungen.Add(b);
            }
            r2.Close();
            con.Close();

            foreach (Patient p in patients)
            {
                foreach (Behandlung b in behandlungen)
                {
                    if (p.ID == b.PatientenID)
                        p.Behandlungen.Add(b);
                }
            }

            return patients;

        }
        static void WriteDatatoXML(List<Patient> patients)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("patients.xml");
            foreach (Patient patient in patients)
            {
                XmlElement p = doc.CreateElement("Patient");
                XmlAttribute d = doc.CreateAttribute("ID");
                d.Value = Convert.ToString(patient.ID);
                p.Attributes.Append(d);
                XmlAttribute last = doc.CreateAttribute("LastName");
                last.Value = patient.Last;
                p.Attributes.Append(last);
                XmlAttribute first = doc.CreateAttribute("FirstName");
                first.InnerText = patient.First;
                p.Attributes.Append(first);
                XmlAttribute sV = doc.CreateAttribute("SVNumber");
                sV.InnerText = patient.SV;
                p.Attributes.Append(sV);
                if (patient.Behandlungen.Count >= 1)
                {
                    XmlElement behanlungen = doc.CreateElement("Behandlungen");
                    foreach (Behandlung behandlung in patient.Behandlungen)
                    {
                        XmlElement b = doc.CreateElement("Behandlung");
                        XmlAttribute date = doc.CreateAttribute("Date");
                        date.InnerText = Convert.ToString(behandlung.Datum);
                        b.Attributes.Append(date);
                        XmlElement description = doc.CreateElement("Behandlungstyp");
                        description.InnerText = behandlung.BehandlungsTyp;
                        b.AppendChild(description);
                        XmlElement duration = doc.CreateElement("Dauer");
                        duration.InnerText = behandlung.Dauer;
                        b.AppendChild(duration);
                        behanlungen.AppendChild(b);
                    }
                    p.AppendChild(behanlungen);
                }
                XmlNode pat = doc.SelectSingleNode("Patients");
                pat.AppendChild(p);
            }
            doc.Save("patients.xml");
        }
    }
}

