﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Sieve_of_Erastothenens
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] boundaries = new int[] { 100, 20 };
            Stopwatch s = Stopwatch.StartNew();



            Thread t1 = new Thread(Erastothenes)
            {
                Name = "BackgroundFred",
                IsBackground = false,
            };
            
            s.Start();
            t1.Start(new Wrapper(100,20));//--->Wrapper Class
            s.Stop();
            Console.WriteLine(s.Elapsed);
            Console.WriteLine(t1.Name+" "+t1.IsBackground);
            //t.Start(new object[] { 100,20 }); --->Object Array




            Thread t2 = new Thread(Erastothenes2)
            {
                Name = "BackgroundFred2",
                IsBackground = false,
            };

            s.Restart();
            t2.Start(boundaries); //--->Object Array
            s.Stop();
            Console.WriteLine(s.Elapsed);
            Console.WriteLine(t2.Name + " " + t2.IsBackground);



            Thread t3 = new Thread(() => Erastothenes3(100, 20))
            {
                Name="Backgroundfred3",
                IsBackground=false,
            };
            s.Restart();
            t3.Start();
            s.Stop();
            Console.WriteLine(s.Elapsed);
            Console.WriteLine(t3.Name+" "+t3.IsBackground);
        }
        static void Erastothenes(object n)
        {
            int highB=0;
            int lowB=0;
            Wrapper w = (Wrapper)n;
            highB = w.HighB;
            lowB = w.LowB; //--->Wrapper Class

            //int[] boundaries = (int[])n;
            //highB = boundaries[0];
            //lowB = boundaries[1]; --->object Array




            bool[] prime = new bool[highB];

            for (int i = 2; i < highB; i++)
                prime[i] = true;

            {
                int i = 2;
                for (; i * i < highB; i++)
                    if (prime[i])
                    {
                        
                        for (int j = i * i; j < highB; j = j + i)
                            prime[j] = false;
                    }
                for (int k = lowB; k < highB; k++)
                    if (prime[k])
                    {
                        Console.WriteLine(k);
                    }
            }
           
            

        }
        static void Erastothenes2(object n)
        {
            int highB = 0;
            int lowB = 0;
            //Wrapper w = (Wrapper)n;
            //highB = w.HighB;
            //lowB = w.LowB; --->Wrapper Class


            highB = ((int[])n)[0];
            lowB = ((int[])n)[1]; //--->object Array




            bool[] prime = new bool[highB];

            for (int i = 2; i < highB; i++)
                prime[i] = true;

            {
                int i = 2;
                for (; i * i < highB; i++)
                    if (prime[i])
                    {
                        
                        for (int j = i * i; j < highB; j = j + i)
                            prime[j] = false;
                    }
                for (int k = lowB; k < highB; k++)
                    if (prime[k])
                    {
                        Console.WriteLine(k);
                    }
            }

        }
        static void Erastothenes3(int highB,int lowB)
        {
            //Wrapper w = (Wrapper)n;
            //highB = w.HighB;
            //lowB = w.LowB; --->Wrapper Class

            //int[] boundaries = (int[])n;
            //highB = boundaries[0];
            //lowB = boundaries[1]; --->object Array




            bool[] prime = new bool[highB];

            for (int i = 2; i < highB; i++)
                prime[i] = true;

            {
                int i = 2;
                for (; i * i < highB; i++)
                    if (prime[i])
                    {
                        for (int j = i * i; j < highB; j = j + i)
                            prime[j] = false;
                    }
                for (int k=lowB; k < highB; k++)
                    if (prime[k])
                    {
                        Console.WriteLine(k);
                    }
            }


        }

 
    
}
    }

