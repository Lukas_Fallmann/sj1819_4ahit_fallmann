﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sieve_of_Erastothenens
{
    class Wrapper
    {
        public int LowB { get; set; }
        public int HighB { get; set; }
        public Wrapper(int highB,int lowB)
        {
            this.HighB = highB;
            this.LowB = lowB;
        }
    }
}
