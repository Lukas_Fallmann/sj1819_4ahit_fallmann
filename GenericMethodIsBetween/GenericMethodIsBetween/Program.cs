﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericMethodIsBetween
{
    class Program
    {
        static void Main(string[] args)
        {
            //testen der Methode mit Int-Werten
            int low = 3;
            int high = 6;
            Console.WriteLine(IsBetween<int>(low,high,3));

            //testen der Methode mit String-Werten
            string shigh = "z";
            string slow = "a";
            //Console.WriteLine(IsBetween(low,shigh,2.46f)); --> nicht möglich, da der Typ der Parameter gleich sein muss !
            Console.WriteLine(IsBetween<string>(slow,shigh,"l"));

            //Testen der Methode mit einer eigenen Klasse die das IComparable Interface implementiert und 
            //in der CompareTo Methode die Nachnamen vergleicht
            Student brndstttr = new Student() { VrNm = "Rene", NchNm = "Brandstetter" ,Grd=4};
            Student ttnur = new Student() { VrNm = "Hannes", NchNm = "Ettenauer", Grd = 3 };
            Student fllmnn = new Student() { VrNm = "Lukas", NchNm = "Fallmann", Grd = 2 };
            Console.WriteLine(IsBetween<Student>(brndstttr,fllmnn,ttnur));
        }

        static bool IsBetween<T>(T low,T high,T Checkvalue) where T:IComparable
        {
            if (low.CompareTo(Checkvalue)<=0 && Checkvalue.CompareTo(high)<=0)
                return true;
            return false;
        }
    }
}
