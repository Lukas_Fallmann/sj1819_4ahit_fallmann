﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericMethodIsBetween
{
    class Student:IComparable
    {
        public string VrNm { get; set; }
        public string NchNm { get; set; }
        public int Grd { get; set; }

        public int CompareTo(object obj)
        {
            Student tmp = (Student)obj;
            if (this.NchNm.CompareTo(tmp.NchNm) == 1)
                return 1;
            else if (this.NchNm.CompareTo(tmp.NchNm) == -1)
                return -1;
            else return 0;

        }
    }
}
